<?php
	class CCashDepositItemsManager {
		var $mysql;
		
		function CCashDepositItemsManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCashDepositItemsList($_cash_deposit_pk) {
			try {
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$this->mysql->orderBy("cash_deposit_items_pk");
		
				$cash_deposit_items_list = $this->mysql->get("cash_deposit_items");
		
				return $cash_deposit_items_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit items list; getCashDepositItemsList(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function insertCashDepositItems($_cash_deposit_pk, $_account_pk, $_deposit_cash, $_cash_deposit_date, $_staff_name, $_store_id) {
			try {
				$newData = array(
					"cash_deposit_pk" => $_cash_deposit_pk,
					"account_pk" => $_account_pk,
					"deposit_cash" => $_deposit_cash,
					"cash_deposit_date" => $_cash_deposit_date,
					"staff_name" => $_staff_name,
					"store_id" => $_store_id,
					"create_date" => date("Y-m-d H:i:s")
				);
		
				$ret = $this->mysql->insert("cash_deposit_items", $newData);
					
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new cash deposit items; insertCashDepositItems(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
		
		function deleteCashDepositItems($_cash_deposit_pk) {
			try {
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$ret = $this->mysql->delete("cash_deposit_items");
					
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to delete cash deposit items; deleteCashDepositItems(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
	}
?>