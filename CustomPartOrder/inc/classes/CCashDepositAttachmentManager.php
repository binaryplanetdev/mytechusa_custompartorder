<?php
	class CCashDepositAttachmentManager {
		var $mysql;
		var $CashDepositManager;
		var $CashDepositLogManager;
				
		function CCashDepositAttachmentManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCashDepositLogManagerClass()
		{
			if ($this->CashDepositLogManager) {
				return;
			}
		
			require_once_classes(Array('CCashDepositLogManager'));
			$this->CashDepositLogManager = new CCashDepositLogManager($this->mysql);
		}
		
		function getCashDepositManagerClass()
		{
			if ($this->CashDepositManager) {
				return;
			}
		
			require_once_classes(Array('CCashDepositManager'));
			$this->CashDepositManager = new CCashDepositManager($this->mysql);
		}
		
		function getCashDepositAttachmentsList($_cash_deposit_pk) {
			try {
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$this->mysql->orderBy("cash_deposit_attachments_pk");
				
				$cash_deposit_list = $this->mysql->get("cash_deposit_attachments");
				
				return $cash_deposit_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit attachments list; getCashDepositAttachmentsList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function insertNewCashDepositAttachments($_cash_deposit_pk, $_uploaded_paths) {
			try {
				$ret_inserted_attachments_pk = array();
				
				foreach ($_uploaded_paths as $file_name) {
					$newData = array(
						"cash_deposit_pk" => $_cash_deposit_pk,
						"file_name" => $file_name,
						"yn_delete" => "N",
						"create_date" => date("Y-m-d H:i:s"),
						"last_update_date" => date("Y-m-d H:i:s")
					);
					
					$ret_inserted_attachments_pk[] = $this->mysql->insert("cash_deposit_attachments", $newData);
				}
																					
				return $ret_inserted_attachments_pk;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new cash deposit attachments; insertNewCashDepositAttachments(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function updateCashDepositAttachmentsStatus($_cash_deposit_pk, $_cash_deposit_attachments_pks, $_yn_delete, $_cash_deposit_detail, $_account_pk, $_staff_name) {
			try {
				$updateData = array(
					"yn_delete" => $_yn_delete,
					"last_update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk)->where("cash_deposit_attachments_pk", $_cash_deposit_attachments_pks, "IN");
				$ret = $this->mysql->update("cash_deposit_attachments", $updateData);
				
				return $updateData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update cash deposit attachments status; updateCashDepositAttachmentsStatus(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
	}
?>