<?php
	class CSKUManager {
		var $mysql;
				
		function CSKUManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function generateSKU() {
			try {
				$ret_insert = $this->mysql->insert("sku", array("val" => "1"));
				if($ret_insert) {
					$ret = $this->mysql->rawQuery("SELECT last_insert_id() AS sku;");
					if($ret != null) {
						return $ret[0]["sku"];
					}
				}
				
				return 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get account list; getAccountList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
	}
?>