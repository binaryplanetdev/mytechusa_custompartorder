<?php
	class CCustomPartOrderLogManager {
		var $mysql;
				
		function CCustomPartOrderLogManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCustomPartOrderLogList($_part_order_pk) {
			try {
				$this->mysql->where("part_order_pk", $_part_order_pk);
				$this->mysql->orderBy("part_order_status_log_pk");
				
				$part_order_log_list = $this->mysql->get("part_order_status_log");
				
				return $part_order_log_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get part order status log list; getCustomPartOrderLogList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function insertNewPartOrderStatusLog($_part_order_pk, $_part_order_status, $_account_pk, $_part_order_staff_name, $_memo) {
			try {
				$newData = array(
					"part_order_pk" => $_part_order_pk,
					"part_order_status" => $_part_order_status,
					"part_order_account_pk" => $_account_pk,
					"part_order_update_staff_name" => $_part_order_staff_name,
					"part_order_memo" => $_memo,
					"part_order_status_log_date" => date("Y-m-d H:i:s")
				);
				
				$ret = $this->mysql->insert("part_order_status_log", $newData);
			
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new part order status log; insertNewPartOrderStatusLog(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
	}
?>