<?php
	class CCashDepositLogManager {
		var $mysql;
				
		function CCashDepositLogManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCashDepositLogList($_cash_deposit_pk) {
			try {
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$this->mysql->orderBy("cash_deposit_log_pk");
				
				$cash_deposit_log_list = $this->mysql->get("cash_deposit_log");
				
				return $cash_deposit_log_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit log list; getCashDepositLogList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function insertCashDepositLog($_cash_deposit_pk, $_before_deposit_cash, $_after_deposit_cash, $_before_cash_deposit_date, $_after_cash_deposit_date, $_before_status, $_after_status, $_update_account_pk, $_update_staff_name, $_memo) {
			try {
				$newData = array(
					"cash_deposit_pk" => $_cash_deposit_pk,
					"before_deposit_cash" => $_before_deposit_cash,
					"after_deposit_cash" => $_after_deposit_cash,
					"before_cash_deposit_date" => $_before_cash_deposit_date,
					"after_cash_deposit_date" => $_after_cash_deposit_date,
					"before_status" => $_before_status,
					"after_status" => $_after_status,
					"memo" => $_memo,
					"update_account_pk" => $_update_account_pk,
					"update_staff_name" => $_update_staff_name,
					"log_date" => date("Y-m-d H:i:s")
				);
				
				$ret = $this->mysql->insert("cash_deposit_log", $newData);
			
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new cash deposit log; insertCashDepositLog(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
	}
?>