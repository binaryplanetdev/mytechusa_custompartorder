<?php
	class CCustomPartOrderManager {
		var $mysql;
		var $CustomPartOrderLogManager;
				
		function CCustomPartOrderManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCustomPartOrderLogManagerClass()
		{
			if ($this->CustomPartOrderLogManager) {
				return;
			}
		
			require_once_classes(Array('CCustomPartOrderLogManager'));
			$this->CustomPartOrderLogManager = new CCustomPartOrderLogManager($this->mysql);		
		}
		
		function getCustomPartOrderList($_store_id) {
			try {
				$this->mysql->where("store_id", $_store_id);
				$this->mysql->orderBy("part_order_pk");
				
				$part_order_list = $this->mysql->get("part_order");
				
				return $part_order_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get part order list; getCustomPartOrderList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getCustomPartOrderAllList() {
			try {
				$this->mysql->orderBy("part_order_pk");
				
				$part_order_list = $this->mysql->get("part_order");
		
				return $part_order_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get part order all list; getCustomPartOrderAllList(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function insertTest($_part_order_pk, $_account_pk, $_store_id, $_part_name, $_part_order_url, $_part_order_quantity, $_part_ticket_number, $_delivery_option, $_tracking_number, $_tracking_url, $_part_order_staff_name, $_part_order_status, $_part_order_price, $_arrival_time, $_total_price, $_shipping_cost) {
			try {
				$newData = array(
						"part_order_pk" => $_part_order_pk,
						"part_order_account_pk" => $_account_pk,
						"store_id" => $_store_id,
						"part_name" => $_part_name,
						"part_order_url" => $_part_order_url,
						"part_order_quantity" => $_part_order_quantity,
						"part_ticket_number" => $_part_ticket_number,
						"delivery_option" => $_delivery_option,
						"tracking_number" => $_tracking_number,
						"tracking_url" => $_tracking_url,
						"part_order_staff_name" => $_part_order_staff_name,
						"part_order_status" => $_part_order_status,
						"part_order_price" => $_part_order_price,
						"arrival_time" => $_arrival_time,
						"total_order_price" => $_total_price, 
						"shipping_cost" => $_shipping_cost,
						"part_order_create_date" => date("Y-m-d H:i:s"),
						"part_order_last_update_date" => date("Y-m-d H:i:s")
				);
		
				$newData["part_order_pk"] = $this->mysql->insert("part_order", $newData);
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new part order; insertNewPartOrder(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
		
		function insertNewPartOrder($_account_pk, $_store_id, $_part_name, $_part_order_url, $_part_order_quantity, $_part_ticket_number, $_delivery_option, $_tracking_number, $_tracking_url, $_part_order_staff_name, 
									$_part_order_status, $_part_order_price, $_arrival_time, $_total_price, $_shipping_cost, $_memo, $_urgent, $_duedate) {
			try {
				$newData = array(
					"part_order_account_pk" => $_account_pk,
					"store_id" => $_store_id,
					"part_name" => $_part_name,
					"part_order_url" => $_part_order_url,
					"part_order_quantity" => $_part_order_quantity,
					"part_ticket_number" => $_part_ticket_number,
					"delivery_option" => $_delivery_option,
					"tracking_number" => $_tracking_number,
					"tracking_url" => $_tracking_url,
					"part_order_staff_name" => $_part_order_staff_name,
					"part_order_status" => $_part_order_status,
					"part_order_price" => $_part_order_price,
					"arrival_time" => $_arrival_time,
					"total_order_price" => $_total_price, 
					"shipping_cost" => $_shipping_cost,
					"urgent" => $_urgent,
					"duedate" => $_duedate,
					"part_order_create_date" => date("Y-m-d H:i:s"),
					"part_order_last_update_date" => date("Y-m-d H:i:s")
				);
				
				$newData["part_order_pk"] = $this->mysql->insert("part_order", $newData);
				if($newData["part_order_pk"]) {					
					$this->getCustomPartOrderLogManagerClass();
					if(empty($_memo)) {
						$_memo = "Add new order";
					}
					
					$this->CustomPartOrderLogManager->insertNewPartOrderStatusLog($newData["part_order_pk"], $_part_order_status, $_account_pk, $_part_order_staff_name, $_memo);
				}
			
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new part order; insertNewPartOrder(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function updatePartOrder($_part_order_pk, $_part_name, $_part_order_url, $_part_order_quantity, $_part_ticket_number, $_delivery_option, $_tracking_number, $_tracking_url, $_part_order_status, $_account_pk, 
								$_part_order_staff_name, $_memo, $_part_order_price, $_arrival_time, $_total_price, $_shipping_cost, $_transaction_cost, $_urgent, $_duedate) {
			try {
				$updateData = array(
					"part_name" => $_part_name,
					"part_order_url" => $_part_order_url,
					"part_order_quantity" => $_part_order_quantity,
					"part_ticket_number" => $_part_ticket_number,
					"delivery_option" => $_delivery_option,
					"tracking_number" => $_tracking_number,
					"tracking_url" => $_tracking_url,
					"transaction_cost" => $_transaction_cost,
					"part_order_status" => $_part_order_status,
					"part_order_price" => $_part_order_price,
					"arrival_time" => $_arrival_time,
					"total_order_price" => $_total_price, 
					"shipping_cost" => $_shipping_cost,
					"urgent" => $_urgent,
					"duedate" => $_duedate,
					"part_order_last_update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("part_order_pk", $_part_order_pk);
				$ret = $this->mysql->update("part_order", $updateData);
				if($ret) {
					$this->getCustomPartOrderLogManagerClass();
					$this->CustomPartOrderLogManager->insertNewPartOrderStatusLog($_part_order_pk, $_part_order_status, $_account_pk, $_part_order_staff_name, $_memo);
				}
				
				$updateData["part_order_pk"] = $_part_order_pk;
				
				return $updateData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update part order status; updatePartOrderStatus(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
		
		function updatePartOrderStatus($_part_order_pk, $_part_order_status, $_account_pk, $_part_order_staff_name, $_memo) {
			try {
				$updateData = array(
					"part_order_status" => $_part_order_status,
					"part_order_last_update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("part_order_pk", $_part_order_pk);
				$ret = $this->mysql->update("part_order", $updateData);
				if($ret) {
					$this->getCustomPartOrderLogManagerClass();
					$this->CustomPartOrderLogManager->insertNewPartOrderStatusLog($_part_order_pk, $_part_order_status, $_account_pk, $_part_order_staff_name, $_memo);
				}
				
				$updateData["part_order_pk"] = $_part_order_pk;
		
				return $updateData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update part order status; updatePartOrderStatus(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
	}
?>