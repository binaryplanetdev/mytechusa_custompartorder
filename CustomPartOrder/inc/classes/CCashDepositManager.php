<?php
	class CCashDepositManager {
		var $mysql;
		var $CashDepositLogManager;
		var $CashDepositAttachmentManager;
		var $CashDepositItemsManager;
				
		function CCashDepositManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getCashDepositLogManagerClass()
		{
			if ($this->CashDepositLogManager) {
				return;
			}
		
			require_once_classes(Array('CCashDepositLogManager'));
			$this->CashDepositLogManager = new CCashDepositLogManager($this->mysql);
		}
		
		function getCashDepositAttachmentManagerClass()
		{
			if ($this->CashDepositAttachmentManager) {
				return;
			}
		
			require_once_classes(Array('CCashDepositAttachmentManager'));
			$this->CashDepositAttachmentManager = new CCashDepositAttachmentManager($this->mysql);
		}
		
		function getCashDepositItemsManagerClass()
		{
			if ($this->CashDepositItemsManager) {
				return;
			}
		
			require_once_classes(Array('CCashDepositItemsManager'));
			$this->CashDepositItemsManager = new CCashDepositItemsManager($this->mysql);
		}
		
		function getCashDepositList($_store_id) {
			try {
				$this->mysql->join("cash_deposit_attachments", "cash_deposit.cash_deposit_pk = cash_deposit_attachments.cash_deposit_pk AND cash_deposit_attachments.yn_delete = 'N'", "LEFT");
				$this->mysql->groupBy("cash_deposit.cash_deposit_pk");
				$this->mysql->orderBy("cash_deposit.cash_deposit_pk");
				$this->mysql->where("cash_deposit.store_id", $_store_id);
				
				$cash_deposit_list = $this->mysql->get("cash_deposit", null, "cash_deposit.*, GROUP_CONCAT(cash_deposit_attachments.cash_deposit_attachments_pk SEPARATOR '|') AS cash_deposit_attachments_pk, GROUP_CONCAT(cash_deposit_attachments.file_name SEPARATOR '|') AS file_name");
				
				return $cash_deposit_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit list; getCashDepositList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getCashDepositAllList() {
			try {
				$this->mysql->join("cash_deposit_attachments", "cash_deposit.cash_deposit_pk = cash_deposit_attachments.cash_deposit_pk AND cash_deposit_attachments.yn_delete = 'N'", "LEFT");
				$this->mysql->groupBy("cash_deposit.cash_deposit_pk");
				$this->mysql->orderBy("cash_deposit.cash_deposit_pk");
				
				$cash_deposit_list = $this->mysql->get("cash_deposit", null, "cash_deposit.*, GROUP_CONCAT(cash_deposit_attachments.cash_deposit_attachments_pk SEPARATOR '|') AS cash_deposit_attachments_pk, GROUP_CONCAT(cash_deposit_attachments.file_name SEPARATOR '|') AS file_name");
		
				return $cash_deposit_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit all list; getCashDepositAllList(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function getCashDepositDetail($_cash_deposit_pk) {
			try {
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$cash_deposit_detail = $this->mysql->getOne("cash_deposit");
			
				return $cash_deposit_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit detail; getCashDepositDetail(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function getCashDepositAttachmentDetail($_cash_deposit_pk) {
			try {
				$this->mysql->join("cash_deposit_attachments", "cash_deposit.cash_deposit_pk = cash_deposit_attachments.cash_deposit_pk AND cash_deposit_attachments.yn_delete = 'N'", "LEFT");
				$this->mysql->where("cash_deposit.cash_deposit_pk", $_cash_deposit_pk);
				$this->mysql->groupBy("cash_deposit.cash_deposit_pk");
				$this->mysql->orderBy("cash_deposit.cash_deposit_pk");
				
				$cash_deposit_detail = $this->mysql->get("cash_deposit", null, "cash_deposit.*, GROUP_CONCAT(cash_deposit_attachments.cash_deposit_attachments_pk SEPARATOR '|') AS cash_deposit_attachments_pk, GROUP_CONCAT(cash_deposit_attachments.file_name SEPARATOR '|') AS file_name");
				
				return $cash_deposit_detail[0];
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get cash deposit attachment detail; getCashDepositAttachmentDetail(" . $_cash_deposit_pk . "); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
		
		function insertNewCashDeposit($_deposit_cash, $_cash_deposit_date, $_status, $_account_pk, $_staff_name, $_store_id, $_memo, $_uploaded_paths, $_deposit_list) {
			try {
				$newData = array(
					"account_pk" => $_account_pk,
					"store_id" => $_store_id,
					"deposit_cash" => $_deposit_cash,
					"status" => $_status,
					"cash_deposit_date" => $_cash_deposit_date,
					"staff_name" => $_staff_name,
					"create_date" => date("Y-m-d H:i:s"),
					"last_update_date" => date("Y-m-d H:i:s")
				);
				
				$newData["cash_deposit_pk"] = $this->mysql->insert("cash_deposit", $newData);
				if($newData["cash_deposit_pk"]) {
					if(empty($_memo)) {
						$_memo = "Add New Data.";
					}
					
					if(count($_uploaded_paths) > 0) {
						$this->getCashDepositAttachmentManagerClass();
						
						$inserted_attachment_pks = $this->CashDepositAttachmentManager->insertNewCashDepositAttachments($newData["cash_deposit_pk"], $_uploaded_paths);
						
						$newData["cash_deposit_attachments_pk"] = implode("|", $inserted_attachment_pks);
						$newData["file_name"] = implode("|", $_uploaded_paths);
						
						$_memo .= "<br/>Upload " . count($inserted_attachment_pks) . " file(s).";
					}
					
					$this->getCashDepositLogManagerClass();
					$this->CashDepositLogManager->insertCashDepositLog($newData["cash_deposit_pk"], null, $_deposit_cash, null, $_cash_deposit_date, null, $_status, $_account_pk, $_staff_name, $_memo);
					
					if($_deposit_list != null) {
						$this->getCashDepositItemsManagerClass();
						
						foreach ($_deposit_list as $row) {
							$this->CashDepositItemsManager->insertCashDepositItems($newData["cash_deposit_pk"], $_account_pk, $row["deposit_cash"], $row["deposit_date"], $_staff_name, $_store_id);
						}
					}
				}
				
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert new cash deposit; insertNewCashDeposit(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function updateCashDeposit($_cash_deposit_pk, $_deposit_cash, $_cash_deposit_date, $_status, $_account_pk, $_staff_name, $_store_id, $_memo, $_deleted_attachment_pks, $_uploaded_paths, $_deposit_list) {
			try {
				$cash_deposit_detail = $this->getCashDepositDetail($_cash_deposit_pk);
				
				$updateData = array(
					"deposit_cash" => $_deposit_cash,
					"status" => $_status,
					"cash_deposit_date" => $_cash_deposit_date,
					"last_update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$ret = $this->mysql->update("cash_deposit", $updateData);
				if($ret) {
					$this->getCashDepositItemsManagerClass();
					$this->getCashDepositAttachmentManagerClass();
					
					if(!empty($_deleted_attachment_pks)) {
						$this->CashDepositAttachmentManager->updateCashDepositAttachmentsStatus($_cash_deposit_pk, $_deleted_attachment_pks, "Y", $cash_deposit_detail, $_account_pk, $_staff_name);
						
						$_memo .= strlen($_memo) > 0 ? "<br/>" : "";
						$_memo .= "Delete " . count($_deleted_attachment_pks) . " file(s).";
					}
					
					if(count($_uploaded_paths) > 0) {
						$updated_attachment_pks = $this->CashDepositAttachmentManager->insertNewCashDepositAttachments($_cash_deposit_pk, $_uploaded_paths);
					
						$_memo .= strlen($_memo) > 0 ? "<br/>" : "";
						$_memo .= "Upload " . count($updated_attachment_pks) . " file(s).";
					}
					
					$this->getCashDepositLogManagerClass();
					$this->CashDepositLogManager->insertCashDepositLog($_cash_deposit_pk, $cash_deposit_detail['deposit_cash'], $_deposit_cash, $cash_deposit_detail['cash_deposit_date'], $_cash_deposit_date, $cash_deposit_detail['status'], $_status, 
							$_account_pk, $_staff_name, $_memo);
					
					$this->CashDepositItemsManager->deleteCashDepositItems($_cash_deposit_pk);
					if($_deposit_list != null) {
						foreach ($_deposit_list as $row) {
							$this->CashDepositItemsManager->insertCashDepositItems($_cash_deposit_pk, $_account_pk, $row["deposit_cash"], $row["deposit_date"], $_staff_name, $_store_id);
						}
					}
				}
				
				$updateData = $this->getCashDepositAttachmentDetail($_cash_deposit_pk);
				
				return $updateData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update cash deposit; updateCashDeposit(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
		
		function updateCashDepositStatus($_cash_deposit_pk, $_status, $_account_pk, $_staff_name, $_store_id, $_memo) {
			try {
				$cash_deposit_detail = $this->getCashDepositDetail($_cash_deposit_pk);
				
				$updateData = array(
					"status" => $_status,
					"last_update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("cash_deposit_pk", $_cash_deposit_pk);
				$ret = $this->mysql->update("cash_deposit", $updateData);
				if($ret) {
					$this->getCashDepositLogManagerClass();
					$this->CashDepositLogManager->insertCashDepositLog($_cash_deposit_pk, $cash_deposit_detail['deposit_cash'], $cash_deposit_detail['deposit_cash'], $cash_deposit_detail['cash_deposit_date'], $cash_deposit_detail['cash_deposit_date'], 
							$cash_deposit_detail['status'], $_status, $_account_pk, $_staff_name, $_memo);
				}
				
				$updateData["cash_deposit_pk"] = $_cash_deposit_pk;
		
				return $updateData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update cash deposit status; updateCashDepositStatus(); ERROR[" . $e->getMessage() . "]");
					
				return null;
			}
		}
	}
?>