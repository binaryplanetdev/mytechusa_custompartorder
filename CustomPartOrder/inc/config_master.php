<?php
	/**
	 * URL
	 */
	define('CONF_URL_INDEX',		CONF_URL_ROOT . "index.php");	
	define('CONF_URL_MENU',			CONF_PATH_ROOT . "menu.php");
	
	define('CONF_URL_LOGIN',		CONF_URL_ROOT . "login.php");
	define('CONF_URL_LOGIN_ACTION',	CONF_URL_ROOT . "login_action.php");
	define('CONF_URL_LOGOUT',		CONF_URL_ROOT . "logout.php");
	
	define('CONF_URL_PART_ORDER_PAGE',	CONF_URL_ROOT . "custom_part_order.php");
	define('CONF_URL_CASH_DEPOSIT_PAGE',	CONF_URL_ROOT . "cash_deposit.php");
	define('CONF_URL_SKU_PAGE',	CONF_URL_ROOT . "sku.php");
	define('CONF_URL_ACCOUNT',		CONF_URL_ROOT . "account.php");
	
	define('CONF_URL_AJAX',			CONF_URL_ROOT . "ajax/ajax.php");
	define('CONF_URL_AJAX_ATTACHMENT',	CONF_URL_ROOT . "ajax/uploadAttachmemt.php");
	define('CONF_URL_AJAX_ATTACHMENT_DELETE',	CONF_URL_ROOT . "ajax/deleteAttachment.php");
	define('CONF_URL_EXPOT',			CONF_URL_ROOT . "exports.php");
	
	$ACCOUNT_ROLE = array("admin", "manager", "technician", "hq_staff");
	$ACCOUNT_STATUS = array("Active", "Suspended", "Deleted");
	$ORDER_STATUS = array("Draft", "New", "Processing", "Shipping", "Received", "Defected", "Pending", "Canceled", "Deleted");
	$CASH_DEPOSIT_STATUS = array("New", "Confirmed", "Canceled", "Deleted");
	
	$STORE_LIST = array(
		0 => "HQ",
		1 => "Suffolk, VA",
		2 => "Lockport, NY",
		3 => "Hampton, VA",
		4 => "Winston-Salem, NC",
		5 => "Greensboro, NC",
		6 => "Norfolk, VA",
		7 => "North Tonawanda, NY",
		8 => "Rochester, NY"
	);
?>