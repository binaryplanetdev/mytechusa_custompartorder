<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CCashDepositManager"));
	
		$session = new CSession();
		$account_info = $session->getLoginData();
			
		if(!isset($account_info)) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		
		$cash_deposit_manager = new CCashDepositManager($mysql_manager->getDb());
		
		if($account_info["role"] == "admin" || $account_info["role"] == "hq_staff") {
			$ret_cash_deposit_list = $cash_deposit_manager->getCashDepositAllList();
		} else {
			$ret_cash_deposit_list = $cash_deposit_manager->getCashDepositList($account_info["store_id"]);
		}
		
		$cash_deposit_list = array();
		if(isset($ret_cash_deposit_list)) {
			foreach ($ret_cash_deposit_list as $row) {
				$cash_deposit_list[$row["cash_deposit_pk"]] = $row;
			}
		}
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-datetimepicker.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/fileinput.css">		
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/fileinput.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/cash_deposit.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var conf_url_login = "<?php echo CONF_URL_LOGIN; ?>";
			var conf_url_ajax = "<?php echo CONF_URL_AJAX; ?>";
			var conf_url_ajax_attachment = "<?php echo CONF_URL_AJAX_ATTACHMENT; ?>";
			var conf_url_ajax_attachment_delete = "<?php echo CONF_URL_AJAX_ATTACHMENT_DELETE; ?>";
			var conf_path_attachments = "<?php echo CONF_URL_ATTACHMENTS; ?>";
			var cash_deposit_list = <?php echo json_encode($cash_deposit_list); ?>;
			var cash_deposit_status = <?php echo json_encode($CASH_DEPOSIT_STATUS); ?>;
			var store_list = <?php echo json_encode($STORE_LIST); ?>;
			var staff_info = <?php echo json_encode($account_info); ?>;
			var list_table = null;
			var last_updated_cash_deposit_pk = null;
			var deleted_attachment_pks = [];
			var action_type = "";

			$(function() {
				list_table = $('#cash_deposit_list').DataTable({
					columns: [
						{ className: "minWidth_50" },
						null,
						null,
						null
					],
					order: [
						[ 0, "desc" ]
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' id='btnAddNewDeposit'>Add Cash Deposit</button>";
						html += "</label>";
						$('#cash_deposit_list_length').html(html);
						
						$('#btnAddNewDeposit').off("click").on('click', function() {
							showCashDepositPopup();
						});

						if(staff_info.role == "admin" || staff_info.role == "hq_staff") {
// 							$('#cash_deposit_list_filter').prepend("<label style='margin-right: 10px;'>Status Filter : <select id='cash_deposit_list_status_filter' class='form-control'></select></label>");
							$('#cash_deposit_list_filter').prepend("<label style='margin-right: 10px;'>Store Filter : <select id='cash_deposit_list_store_filter' class='form-control'></select></label>");
							
							$('#cash_deposit_list_status_filter').off("change").on('change', function() {
								statusFilter($("#cash_deposit_list_status_filter").val());
							});

							$('#cash_deposit_list_store_filter').off("change").on('change', function() {
								storeFilter($("#cash_deposit_list_store_filter").val());
							});

							setStatusFilter();
							setStroeFilter();
						}
					},
					drawCallback: function() {						
						$('#cash_deposit_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#cash_deposit_list > tbody > tr");

							var isOpenPopup = false;
							for(var i = 0; i < 4; i++) {
								var key = '#cash_deposit_list > tbody > tr:eq(' + index + ') > td:eq(' + i + ')';
								if($(event.target).is(key)) {
									isOpenPopup = true;
									break;
								}
							}

							if(isOpenPopup) {
								var cash_deposit_pk = $(this).attr("id");
						        var cash_deposit_data = cash_deposit_list[cash_deposit_pk];

						        if(cash_deposit_data) {
							        showCashDepositPopup(cash_deposit_data);
						        }
							}
						});

						$(".btnDeleteDeposit").off("click").on("click", function() {
							var result = confirm("Are you sure delete selected deposit?");
							if(result) {
								var cash_deposit_pk = $(this).parent("td").parent("tr").attr("id");
								deleteCashDeposit(cash_deposit_pk);
							}
						});
					}
				});
				
				$.each(cash_deposit_list, function(key, val) {
					var new_node = list_table.row.add([
						val.cash_deposit_date,
						val.deposit_cash,
						store_list[val.store_id],
						val.staff_name
					]).node();

					$(new_node).attr("id", val.cash_deposit_pk);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div class="loading">Loading</div>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Cash Deposit</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Cash Deposit List</div>
							<div class="panel-body">
								<div class="dataTable_wrapper table-responsive">
									<table class="table table-striped table-bordered table-hover responsive" id="cash_deposit_list">
										<thead>
											<tr>
												<th>Deposit Date</th>
												<th>Amount</th>
												<th>Store Name</th>
												<th>Staff Name</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>