function showCashDepositPopup(_cash_deposit_data) {
	deleted_attachment_pks = [];
	
	var mode = _cash_deposit_data == null ? "create" : "edit";
	
	$('#modal_popup_content').html(getCashDepositPopupHtml(mode));
	$('#modal_popup_footer').html(getCashDepositPopupFooterHtml(mode));
	
	if(mode == "edit") {
		$('#modal_popup_label').text("Cash Deposit Detail");
		
		$('#btnSaveChanges').off("click").on("click", function() {
			updateCashDeposit(_cash_deposit_data.cash_deposit_pk);
		});
	} else {
		$('#modal_popup_label').text("Add Cash Deposit");
		
		$('#btnCreateCashDeposit').off("click").on("click", function() {
			addCashDeposit();
		});
	}
	
	setCashDepositStatus();			
	initCashDepositData(mode, _cash_deposit_data);
	
	$('#cash_deposit_date_warp').datetimepicker({
		format: "MM/DD/YYYY",
		allowInputToggle: true,
		maxDate: new Date()
	});
	
	$("#cash_deposit_list_date_warp").datetimepicker({
		format: "MM/DD/YYYY",
		allowInputToggle: true,
		maxDate: new Date()
	});
	
	var registeredAttachments = getRegisteredAttachments(_cash_deposit_data);
	
	$("#attachment").fileinput({
		uploadUrl: conf_url_ajax,
		uploadAsync: false,
		maxFileCount: 10,
		maxFileSize: 10240,
		overwriteInitial: false,
		showUpload: false,
		showRemove: false,
		showClose: false,
		dropZoneEnabled: false,
		browseClass: "btn btn-success",
		browseLabel: "Files",
		browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
		removeClass: "btn btn-danger",
		removeLabel: "Delete",
		removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
		layoutTemplates: {
			actions: "<div class='file-actions'>\n" +
					"    <div class='file-footer-buttons'>\n" +
					"        {delete}" +
					"    </div>\n" +
					"    <div class='clearfix'></div>\n" +
					"</div>",
			actionUpload : ""
		},
		initialPreview: registeredAttachments.initialPreview,
		initialPreviewConfig: registeredAttachments.initialPreviewConfig,
		uploadExtraData: function() {
			var params = {};
			params.type = action_type;
			params.cash_deposit_pk = last_updated_cash_deposit_pk;
			params.cash_deposit_date = $.trim($('#cash_deposit_date').val());
			params.deposit_cash = $.trim($('#deposit_cash').val());
			params.status = $.trim($('#status').val());
			params.memo = $.trim($("#memo").val());
			params.deleted_attachment_pks = deleted_attachment_pks;
			params.deposit_list = JSON.stringify(getDepositList());
			
            return params;
        }
	});
	
	$('#attachment').on('filebatchuploadsuccess', function(event, data, previewId, index) {		
		$('.loading').hide();
		
		$('#modal_popup').modal('hide');
		
		var ret_data = data.response;
		if(ret_data.result == "OK") {
			cash_deposit_list[ret_data.data.cash_deposit_pk] = ret_data.data;
			deleted_attachment_pks = [];

			if(action_type == "add_cash_deposit") {
				addNewTableRow(cash_deposit_list[ret_data.data.cash_deposit_pk]);	
			} else {
				updateTableRow(cash_deposit_list[ret_data.data.cash_deposit_pk]);
			}
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	});	
	
	$('#attachment').on('filebatchuploaderror', function(event, data, msg) {
		$('.loading').hide();
		
		$('#modal_popup').modal('hide');
		
		alert("Error while uploading images.[" + msg + "]");
	});
		
	$('#attachment').on('filedeleted', function(event, key) {
		deleted_attachment_pks.push(key);
	});
	
	$(".file-preview-image").off("click").on("click", function() {
		window.open($(this).attr("src"), "_blank");
	});
	
	if(mode == "edit" && staff_info.role == "manager") {
		$('#attachment').fileinput('disable');
	}
	
	$("#btnAddDepositList").off("click").on("click", function() {
		var deposit_list_date = $.trim($("#deposit_list_date").val());
		var deposit_list_cash = $.trim($("#deposit_list_cash").val());
		
		if(deposit_list_date.length <= 0) {
			alert("Deposit Date is Required!");
			return;
		}
		
		if(deposit_list_cash == 0 || deposit_list_cash.length <= 0) {
			alert("Deposit Cash is Required!");
			return;
		}
		
		var html = "";
		
		html += "<tr>";
		html += "	<td>" + deposit_list_date + "</td>";
		html += "	<td>" + deposit_list_cash + "</td>";
		html += "	<td><button type='button' class='btn btn-warning btn-circle btnDeleteDepositItem'><i class='fa fa-times'></i></button></td>";
		html += "</tr>";
		
		$("#deposit_list_tbody").append(html);
		
		$("#deposit_list_date").val("");
		$("#deposit_list_cash").val("");
		
		setTotalDepositCash();
		
		$(".btnDeleteDepositItem").off("click").on("click", function() {
			$(this).parent("td").parent("tr").remove();
			
			setTotalDepositCash();
		});
	});
	
	$('#modal_popup').modal('show');
}

function setTotalDepositCash() {
	var totalCash = 0;
	
	$.each($("#deposit_list_tbody > tr"), function(_key, _val) {
		var cash = parseFloat($(this).children("td:eq(1)").text());
		totalCash += cash;
	});
	
	$("#deposit_cash").val(totalCash);
}

function getRegisteredAttachments(_cash_deposit_data) {
	var retAttachments = {};
	retAttachments.initialPreview = [];
	retAttachments.initialPreviewConfig = [];
	
	if(_cash_deposit_data == null || !_cash_deposit_data.cash_deposit_attachments_pk) {
		return retAttachments;
	}
	
	var attachments_pks = _cash_deposit_data.cash_deposit_attachments_pk.split("|");
	var attachments_files = _cash_deposit_data.file_name.split("|");
	var attachments_size = attachments_pks.length;
	
	for(var i = 0; i < attachments_size; i++) {
		retAttachments.initialPreview.push("<img src='" + conf_path_attachments + attachments_files[i] + "' class='file-preview-image' style='cursor:pointer;'>");
		
		var tmp_name = attachments_files[i].split("/");
		retAttachments.initialPreviewConfig.push(
			{ caption: tmp_name[tmp_name.length - 1], width: "auto", url: conf_url_ajax_attachment_delete, key: attachments_pks[i] }
		);
	}
	
	return retAttachments;
}

function changeAttachPreview() {
	var files = $('#attachment').fileinput('getFileStack');
	
	if(files.length <= 0) {
		$('.file-preview').hide();
	} else {
		$('.file-preview').show();
	}
}

function initCashDepositData(_mode, _cash_deposit_data) {
	if(_mode == "edit") {
		$('#cash_deposit_date').val(_cash_deposit_data.cash_deposit_date);
		$('#deposit_cash').val(_cash_deposit_data.deposit_cash);
		$('#staff_name').val(_cash_deposit_data.staff_name);
		$('#store_name').val(store_list[_cash_deposit_data.store_id]);
		$('#status').val(_cash_deposit_data.status);
		
		if(staff_info.role == "admin") {
			$('#cash_deposit_date').attr("readonly", false);
			$('#deposit_cash').attr("readonly", false);
		} else {
			$('#cash_deposit_date').attr("readonly", true);
			$('#deposit_cash').attr("readonly", true);
		}
		
		$('#status').attr("disabled", false);
		
		getCashDepositLogList(_cash_deposit_data.cash_deposit_pk);
		getCashDepositItemsList(_cash_deposit_data.cash_deposit_pk);
	} else {
		$('#cash_deposit_date').val(getTodayDepositDate());
		$('#deposit_cash').val(0);
		$('#staff_name').val(staff_info.staff_name);
		$('#store_name').val(store_list[staff_info.store_id]);
		
		$('#status').attr("disabled", true);
	}
	
	$('#deposit_cash').attr("readonly", true);
	$('#staff_name').attr("readonly", true);
	$('#store_name').attr("readonly", true);
}

function getCashDepositLogList(_cash_deposit_pk) {
	var params = {};
	params.type = "get_cash_deposit_log";
	params.cash_deposit_pk = _cash_deposit_pk;
	
	$.post(conf_url_ajax, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			var html = "";
			
			$.each(ret_data.data, function(key, val) {
				html += "<tr>";
				html += "<td>" + val.log_date + "</td>";
				html += "<td>" + val.after_cash_deposit_date + "</td>";
				html += "<td>" + val.after_deposit_cash + "</td>";
				html += "<td>" + val.update_staff_name + "</td>";
				html += "<td class='memo_max_width'>" + val.memo + "</td>";
				html += "</tr>";
			});
			
			$("#memo_tbody").html(html);
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function getCashDepositItemsList(_cash_deposit_pk) {
	var params = {};
	params.type = "get_cash_deposit_items";
	params.cash_deposit_pk = _cash_deposit_pk;
	
	$.post(conf_url_ajax, params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			var html = "";
			
			$.each(ret_data.data, function(key, val) {
				html += "<tr>";
				html += "	<td>" + val.cash_deposit_date + "</td>";
				html += "	<td>" + val.deposit_cash + "</td>";
				html += "	<td><button type='button' class='btn btn-warning btn-circle btnDeleteDepositItem' data-pk='" + val.cash_deposit_items_pk + "'><i class='fa fa-times'></i></button></td>";
				html += "</tr>";
				
			});
			
			$("#deposit_list_tbody").html(html);
			
			$(".btnDeleteDepositItem").off("click").on("click", function() {
				var cash_deposit_items_pk = $(this).attr("data-pk"); 
				$(this).parent("td").parent("tr").remove();
				
				setTotalDepositCash();
			});
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function setCashDepositStatus() {
	var html = "";
	
	$.each(cash_deposit_status, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#status').html(html);
}

function getCashDepositPopupHtml(_mode) {
    var html = "";
    html += "<div class='row'>";
    html += "    <form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='cash_deposit_date'>";
	html += "           	<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Deposit Date";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='input-group date' id='cash_deposit_date_warp'>";
	html += "					<input type='text' class='form-control' id='cash_deposit_date' placeholder='Date' data-date-end-date='0d'/>";
	html += "						<span class='input-group-addon'>";
	html += "							<span class='glyphicon glyphicon-calendar'></span>";
	html += "						</span>";
	html += "				</div>";
	html += "				<p class='help-block'>e.g. MM/DD/YYYY</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='deposit_cash'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Deposit Cash";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='deposit_cash' placeholder='Cash'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label'></label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='row'>";
	html += "					<div class='panel panel-success'>";
	html += "						<div class='panel-body'>";
	html += "							<div class='form-group form-group-md'>";
	html += "								<label class='col-sm-3 control-label' for='deposit_list_date'>";
	html += "       	        					<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Deposit Date";
	html += "								</label>";
	html += "								<div class='col-sm-8'>";
	html += "               		 			<div class='input-group date' id='cash_deposit_list_date_warp'>";
	html += "                   					<input type='text' class='form-control' id='deposit_list_date' placeholder='Date' data-date-end-date='0d'/>";
	html += "											<span class='input-group-addon'>";
	html += "		                        				<span class='glyphicon glyphicon-calendar'></span>";
	html += "	    		                			</span>";
	html += "   	        		     			</div>";
	html += "       	         					<p class='help-block'>e.g. MM/DD/YYYY</p>";
	html += "		           	 			</div>";
	html += "       		 			</div>";
	html += "							<div class='form-group form-group-md'>";
	html += "            					<label class='col-sm-3 control-label' for='deposit_list_cash'>";
	html += "  	             					<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Deposit Cash";
	html += "		       	     			</label>";
	html += "      		    	 			<div class='col-sm-5'>";
	html += "              			 			<input type='text' class='form-control' id='deposit_list_cash' placeholder='Cash'/>";
	html += "        						</div>";
	html += "		            			<div class='col-sm-3'>";
	html += "  			             			<button type='button' class='btn btn-primary' id='btnAddDepositList'>Add</button>";
	html += "      	    		 			</div>";
	html += "   						</div>";
	html += "							<div class='table-responsive'>";
	html += "								<table class='table table-striped'>";
	html += "									<thead>";
	html += "										<tr>";
	html += "											<th>Deposit Date</th>";
	html += "											<th>Deposit Cash</th>";
	html += "											<th>Deletion</th>";
	html += "										</tr>";
	html += "									</thead>";
	html += "									<tbody id='deposit_list_tbody'></tbody>";
	html += "								</table>";
	html += "							</div>";
	html += "						</div>";
	html += "					</div>";
	html += "				</div>";	
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='staff_name'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Staff Name";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='staff_name'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='store_name'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Store Name";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='store_name'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='attachment'>Deposit Receipt</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='file' class='file' id='attachment' name='attachment[]' multiple/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md' style='display:none;'>";
	html += "			<label class='col-sm-3 control-label' for='status'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Status";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='status'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='memo'>Memo</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' rows='3' id='memo' placeholder='Note'></textarea>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	
	if(_mode == "edit") {
		html += "<div class='row'>";
		html += "	<div class='col-lg-12'>";
		html += "		<div class='panel panel-success'>";
		html += "			<div class='panel-heading'>Change Log</div>";
		html += "			<div class='panel-body'>";
		html += "				<div class='table-responsive'>";
		html += "					<table class='table table-striped'>";
		html += "						<thead>";
		html += "							<tr>";
		html += "								<th>Log Date</th>";
		html += "								<th>Deposit Date</th>";
		html += "								<th>Deposit Cash</th>";
		html += "								<th>Staff</th>";
		html += "								<th>Memo</th>";
		html += "							</tr>";
		html += "						</thead>";
		html += "						<tbody id='memo_tbody'></tbody>";
		html += "					</table>";
		html += "				</div>";
		html += "			</div>";
		html += "		</div>";
		html += "	</div>";
		html += "</div>";
	}
	
	html += "</div>";
	
	return html;
}

function getCashDepositPopupFooterHtml(_mode) {
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button>";
	
	if(_mode == "edit") {
		footer_html += "<button type='button' class='btn btn-primary' id='btnSaveChanges'>Save Changes</button>";
	} else {
		footer_html += "<button type='button' class='btn btn-primary' id='btnCreateCashDeposit'>Add</button>";
	}
	
	return footer_html;
}

function statusFilter(_status) {
	if(_status != "All") {
		list_table.columns(1).search(_status).draw();
	} else {
		list_table.columns(1).search("").draw();
	}
}

function setStatusFilter() {
	var html = "";
	
	html += "<option value='All'>All</option>";
	$.each(cash_deposit_status, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#cash_deposit_list_status_filter').html(html);
}

function storeFilter(_store) {
	if(_store != "All") {
		list_table.columns(2).search(_store).draw();
	} else {
		list_table.columns(2).search("").draw();
	}
}

function setStroeFilter() {
var html = "";
	
	html += "<option value='All'>All</option>";
	$.each(store_list, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#cash_deposit_list_store_filter').html(html);
}

function updateAttachFiles(_cash_deposit_pk) {
	last_updated_cash_deposit_pk = _cash_deposit_pk;
	
	$('.loading').show();
	
	$('#attachment').fileinput('enable').fileinput('upload');
}

function getDepositList() {
	var deposit_list = [];
	
	$.each($("#deposit_list_tbody > tr"), function(_key, _val) {
		var deposit_date = $(this).children("td:eq(0)").text();
		var deposit_cash = parseFloat($(this).children("td:eq(1)").text());
		
		deposit_list.push({
			deposit_date: deposit_date,
			deposit_cash: deposit_cash
		});
	});
	
	return deposit_list;
}

function addCashDeposit() {
	var params = {};
	params.type = "add_cash_deposit";
	params.cash_deposit_date = $.trim($('#cash_deposit_date').val());
	params.deposit_cash = $.trim($('#deposit_cash').val());
	params.status = $.trim($('#status').val());
	params.memo = $.trim($("#memo").val());
	
	if(params.cash_deposit_date.length <= 0) {
		alert("Deposit Date is Required!");
		return;
	}
	
	if(params.deposit_cash.length <= 0) {
		alert("Deposit Cash is Required!");
		return;
	}
	
	if(params.status.length <= 0) {
		alert("Status is Required!");
		return;
	}
	
	action_type = "add_cash_deposit";
	
	updateAttachFiles();
}

function updateCashDeposit(_cash_deposit_pk) {
	var params = {};
	params.type = "update_cash_deposit";
	params.cash_deposit_pk = _cash_deposit_pk;
	params.cash_deposit_date = $.trim($('#cash_deposit_date').val());
	params.deposit_cash = $.trim($('#deposit_cash').val());
	params.status = $.trim($('#status').val());
	params.memo = $.trim($("#memo").val());
	params.deleted_attachment_pks = deleted_attachment_pks;
	
	if(params.cash_deposit_date.length <= 0) {
		alert("Deposit Date is Required!");
		return;
	}
	
	if(params.deposit_cash.length <= 0) {
		alert("Deposit Cash is Required!");
		return;
	}
	
	if(params.status.length <= 0) {
		alert("Status is Required!");
		return;
	}
	
	action_type = "update_cash_deposit";
	
	updateAttachFiles(_cash_deposit_pk);
}

function deleteCashDeposit(_cash_deposit_pk) {
	var params = {};
	params.type = "update_cash_deposit_status";
	params.cash_deposit_pk = _cash_deposit_pk;
	params.status = "Deleted";
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			cash_deposit_list[_cash_deposit_pk].status = ret_data.data.status;
			
			updateTableRow(cash_deposit_list[_cash_deposit_pk]);

			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function updateTableRow(_data) {
	list_table.row($('#' + _data.cash_deposit_pk)).data([
		_data.cash_deposit_date,
		_data.deposit_cash,
		store_list[_data.store_id],
		_data.staff_name
	]).draw(false);
}

function addNewTableRow(_data) {
	var new_node = list_table.row.add([
		_data.cash_deposit_date,
		_data.deposit_cash,
		store_list[_data.store_id],
		_data.staff_name
	]).draw(false).node();

	$(new_node).attr("id", _data.cash_deposit_pk);
}

function getTodayDepositDate() {
	var now = new Date();
	
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	month = month < 10 ? "0" + month : month;
	var date = now.getDate();
	date = date < 10 ? "0" + date : date;
	
	return month + "/" + date + "/" + year;
}
