function showAccountPopup(_account_data) {
	var mode = _account_data == null ? "create" : "edit";
	
	var html = "";
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='account_id'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>ID";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='account_id' placeholder='ID' required/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='account_pw'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Password";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='password' class='form-control' id='account_pw' placeholder='Password' required/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='account_pw_confirm'>";
//	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Password Confirm";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='password' class='form-control' id='account_pw_confirm' placeholder='Password Confirm' required/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='staff_name'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Staff Name";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='staff_name' placeholder='Name' required/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='store'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Store";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='store'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='role'>Role</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='role'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='account_status'>Status</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='account_status'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	$('#modal_popup_content').html(html);
	
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button>";
	
	if(mode == "edit") {
		$('#modal_popup_label').text("Account Detail");
		footer_html += "<button type='button' class='btn btn-primary' id='btnSaveChanges'>Save Changes</button>";
	} else {
		$('#modal_popup_label').text("Add Account");
		footer_html += "<button type='button' class='btn btn-primary' id='btnAddNewAccount'>Add</button>";
	}
	
	$('#modal_popup_footer').html(footer_html);
	
	if(mode == "edit") {
		$('#btnSaveChanges').off("click").on("click", function() {
			updateAccount(_account_data.part_order_account_pk);
		});
	} else {
		$('#btnAddNewAccount').off("click").on("click", function() {
			addAccount();
		});
	}
	
	setAccountRole();
	setStore();
	setAccountStatus();
	
	if(mode == "edit") {
		$('#account_id').val(_account_data.account_id);
		$('#staff_name').val(_account_data.staff_name);
		$('#role').val(_account_data.role);
		$('#store').val(_account_data.store_id);
		$('#account_status').val(_account_data.account_status);
	}
}

function setAccountStatus() {
	var html = "";
	$.each(account_status_list, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#account_status').html(html);
}

function setAccountRole() {
	var html = "";
	$.each(account_role, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#role').html(html);
}

function setStore() {
	var html = "";
	$.each(store_list, function(key, val) {
		html += "<option value='" + key + "'>" + val + "</option>";
	});
	
	$('#store').html(html);
}

function updateAccount(_part_order_account_pk) {
	var params = {};
	params.type = "update_account";
	params.part_order_account_pk = _part_order_account_pk;
	params.account_id = $.trim($('#account_id').val());
	params.account_pw = $.trim($('#account_pw').val());
	params.account_pw_confirm = $.trim($('#account_pw_confirm').val());
	params.staff_name = $.trim($('#staff_name').val());
	params.store_id = $.trim($('#store').val());
	params.role = $.trim($('#role').val());
	params.account_status = $.trim($('#account_status').val());
	
	if(params.account_id.length <= 0) {
		alert("ID is Required!");
		return;
	}
	
	if(params.account_pw.length > 0 && params.account_pw != params.account_pw_confirm) {
		alert("Password is not same!");
		return;
	}
	
	if(params.staff_name.length <= 0) {
		alert("Staff Name is Required!");
		return;
	}
	
	if(params.store_id.length <= 0) {
		alert("Store is Required!");
		return;
	}
	
	if(params.role.length <= 0) {
		alert("Role is Required!");
		return;
	}
	
	if(params.account_status.length <= 0) {
		alert("Account Status is Required!");
		return;
	}
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		$('#modal_popup').modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			account_list[ret_data.data.part_order_account_pk] = ret_data.data;
			
			var deleteAccountHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteAccount' " + (account_list[_part_order_account_pk].account_status == "Deleted" ? "disabled" : "") + "><i class='fa fa-times'></i></button>";
			
			list_table.row($('#' + _part_order_account_pk)).data([
  				account_list[_part_order_account_pk].account_id,
  				account_list[_part_order_account_pk].staff_name,
				store_list[account_list[_part_order_account_pk].store_id],
				account_list[_part_order_account_pk].role,
				account_list[_part_order_account_pk].account_status,
				account_list[_part_order_account_pk].create_date,
				deleteAccountHtml
           	]).draw(false);
			
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function addAccount() {
	var params = {};
	params.type = "add_account";
	params.account_id = $.trim($('#account_id').val());
	params.account_pw = $.trim($('#account_pw').val());
	params.account_pw_confirm = $.trim($('#account_pw_confirm').val());
	params.staff_name = $.trim($('#staff_name').val());
	params.store_id = $.trim($('#store').val());
	params.role = $.trim($('#role').val());
	params.account_status = $.trim($('#account_status').val());
		
	if(params.account_id.length <= 0) {
		alert("ID is Required!");
		return;
	}
	
	if(params.account_pw.length <= 0) {
		alert("Password is Required!");
		return;
	} else if(params.account_pw != params.account_pw_confirm) {
		alert("Password is not same!");
		return;
	}
	
	if(params.staff_name.length <= 0) {
		alert("Staff Name is Required!");
		return;
	}
	
	if(params.store_id.length <= 0) {
		alert("Store is Required!");
		return;
	}
	
	if(params.role.length <= 0) {
		alert("Role is Required!");
		return;
	}
	
	if(params.account_status.length <= 0) {
		alert("Account Status is Required!");
		return;
	}
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		$('#modal_popup').modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			account_list[ret_data.data.part_order_account_pk] = ret_data.data;
			
			var deleteAccountHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteAccount' " + (ret_data.data.account_status == "Deleted" ? "disabled" : "") + "><i class='fa fa-times'></i></button>";
			
			var new_node = list_table.row.add([
				ret_data.data.account_id,
				ret_data.data.staff_name,
				store_list[ret_data.data.store_id],
				ret_data.data.role,
				ret_data.data.account_status,
				ret_data.data.create_date,
				deleteAccountHtml
			]).draw(false).node();

			$(new_node).attr("id", ret_data.data.part_order_account_pk);
			
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function deleteAccount(_part_order_account_pk) {
	var params = {};
	params.type = "update_account_status";
	params.part_order_account_pk = _part_order_account_pk;
	params.account_status = "Deleted";
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			account_list[_part_order_account_pk].account_status = params.account_status;
			
			var deleteAccountHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteAccount' " + (account_list[_part_order_account_pk].account_status == "Deleted" ? "disabled" : "") + "><i class='fa fa-times'></i></button>";
			
			list_table.row($('#' + _part_order_account_pk)).data([
  				account_list[_part_order_account_pk].account_id,
  				account_list[_part_order_account_pk].staff_name,
				store_list[account_list[_part_order_account_pk].store_id],
				account_list[_part_order_account_pk].role,
				account_list[_part_order_account_pk].account_status,
				account_list[_part_order_account_pk].create_date,
				deleteAccountHtml
           	]).draw(false);

			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

