function showPartOrderPopup(_part_order_data) {
	var mode = _part_order_data == null ? "create" : "edit";
	
	$('#modal_popup_content').html(getPartOrderPopupHtml(_part_order_data, mode));
	$('#modal_popup_footer').html(getPartOrderPopupFooterHtml(mode));
	
	if(mode == "edit") {
		$('#modal_popup_label').text("Part Order Detail");
		
		if(staff_info.role == "manager" || staff_info.role == "technician") {
			setPartOrderStatus(_part_order_data.part_order_status);	
		} else {
			setPartOrderStatus();
		}
		
		$('#btnSaveChanges').off("click").on("click", function() {
			updatePartOrderStatus(_part_order_data.part_order_pk);
		});
	} else {
		$('#modal_popup_label').text("Add Part Order");
		
		setPartOrderStatus();
		
		$('#btnCreatePartOrder').off("click").on("click", function() {
			addNewPartOrder();
		});
	}
	
	initOrderData(mode, _part_order_data);
	
	$('#part_order_url').off("keyup").on("keyup", function() {
		var url = $.trim($(this).val());
		if(url.length > 0) {
			$('#btnPopupURL').attr("disabled", false);	
		} else {
			$('#btnPopupURL').attr("disabled", true);
		}
	});
	
	$('#btnPopupURL').off("click").on("click", function() {
		var url = $.trim($('#part_order_url').val());
		if(url.length > 0) {
			if(!url.startsWith("http")) {
				url = "http://" + url;
			}
			
			window.open(url, "_blank");
		}
	});
	
	$('#tracking_url').off("keyup").on("keyup", function() {
		var url = $.trim($(this).val());
		if(url.length > 0) {
			$('#btnPopupTrackingURL').attr("disabled", false);	
		} else {
			$('#btnPopupTrackingURL').attr("disabled", true);
		}
	});
	
	$('#btnPopupTrackingURL').off("click").on("click", function() {
		var url = $.trim($('#tracking_url').val());
		if(url.length > 0) {
			if(!url.startsWith("http")) {
				url = "http://" + url;
			}
			
			window.open(url, "_blank");
		}
	});
	
	$("#urgent").off("click").on("click", function() {
		if($(this).is(":checked")) {
			$(".duedate-wrapper").removeClass("hidden");
		} else {
			$(".duedate-wrapper").addClass("hidden");
			$("#duedate").val("");
		}
	});
}

function getPartOrderPopupFooterHtml(_mode) {
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button>";
	
	if(_mode == "edit") {
		footer_html += "<button type='button' class='btn btn-primary' id='btnSaveChanges'>Save Changes</button>";
	} else {
		footer_html += "<button type='button' class='btn btn-primary' id='btnCreatePartOrder'>Order</button>";
	}
	
	return footer_html;
}

function getPartOrderPopupHtml(_part_order_data, _mode) {
	var html = "";
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_name'>Part Name</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='part_name' placeholder='Part Name'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_order_url'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>URL";
	html += "			</label>";
	html += "			<div class='col-sm-7'>";
	html += "				<input type='text' class='form-control' id='part_order_url' placeholder='Part Order URL'/>";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button type='button' class='btn btn-primary' id='btnPopupURL'><i class='fa fa-external-link'></i></button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_order_quantity'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Quantity";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='part_order_quantity' placeholder='Part Order Quantity'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_ticket_number'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Ticket Number";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='part_ticket_number' placeholder='Ticket Number'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='delivery_option'>Delivery Option</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='delivery_option' placeholder='Delivery Option'/>";
	html += "				<p class='help-block'>Please indicate if a customer wants to pay for quick delivery, otherwise, we will order with free delivery.</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='urgent'>Urgent</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='checkbox'>";
	html += "					<label>";
	html += "						<input type='checkbox' value='Y' id='urgent'>Urgent";
	html += "					</label>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md hidden duedate-wrapper'>";
	html += "			<label class='col-sm-3 control-label' for='duedate'>Due Date</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='duedate' placeholder='Due Date'>";
	html += "				<p class='help-block'>Due Date</p>";
	html += "			</div>";
	html += "		</div>";
	
	if(_mode == "edit") {
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='tracking_number'>Tracking Number</label>";
		html += "			<div class='col-sm-8'>";
		html += "				<input type='text' class='form-control' id='tracking_number' placeholder='Order Tracking Number'/>";
		html += "			</div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='tracking_url'>Tracking URL</label>";
		html += "			<div class='col-sm-7'>";
		html += "				<input type='text' class='form-control' id='tracking_url' placeholder='Order Tracking URL'/>";
		html += "			</div>";
		html += "			<div class='col-sm-2'>";
		html += "				<button type='button' class='btn btn-primary' id='btnPopupTrackingURL'><i class='fa fa-external-link'></i></button>";
		html += "			</div>";
		html += "		</div>";
		if(_part_order_data.part_order_status != "New") {
			html += "		<div class='form-group form-group-md'>";
			html += "			<label class='col-sm-3 control-label' for='arrival_time'>Estimate Arrival Time</label>";
			html += "			<div class='col-sm-8'>";
			html += "				<input type='text' class='form-control' id='arrival_time' placeholder='Estimate Arrival Time'/>";
			html += "			</div>";
			html += "		</div>";
		}
	}
	
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_order_status'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Order Status";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='part_order_status'></select>";
	html += "			</div>";
	html += "		</div>";
	
	if(_mode == "edit") {
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='transaction_cost'>Transaction Cost</label>";
		html += "			<div class='col-sm-8'>";
		html += "				<input type='text' class='form-control' id='transaction_cost' placeholder='Transaction Cost'/>";
		html += "			</div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='part_order_price'>Unit Price</label>";
		html += "			<div class='col-sm-8'>";
		html += "				<input type='text' class='form-control' id='part_order_price' placeholder='Unit Price'/>";
		html += "			</div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='shipping_cost'>Shipping Cost</label>";
		html += "			<div class='col-sm-8'>";
		html += "				<input type='text' class='form-control' id='shipping_cost' placeholder='Shipping Cost'/>";
		html += "			</div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='total_order_price'>Total Cost</label>";
		html += "			<div class='col-sm-8'>";
		html += "				<input type='text' class='form-control' id='total_order_price' placeholder='Total Cost'/>";
		html += "			</div>";
		html += "		</div>";
	}
	
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_order_staff_name'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Staff Name";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='part_order_staff_name' readonly/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='part_order_store_name'>";
	html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Store Name";
	html += "			</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='part_order_store_name' readonly/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='memo'>Memo</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' rows='3' id='memo' placeholder='Note'></textarea>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	
	if(_mode == "edit") {
		html += "<div class='row'>";
		html += "	<div class='col-lg-12'>";
		html += "		<div class='panel panel-success'>";
		html += "			<div class='panel-heading'>Status Log</div>";
		html += "			<div class='panel-body'>";
		html += "				<div class='table-responsive'>";
		html += "					<table class='table table-striped'>";
		html += "						<thead>";
		html += "							<tr>";
		html += "								<th>Date</th>";
		html += "								<th>Status</th>";
		html += "								<th>Staff</th>";
		html += "								<th>Memo</th>";
		html += "							</tr>";
		html += "						</thead>";
		html += "						<tbody id='memo_tbody'></tbody>";
		html += "					</table>";
		html += "				</div>";
		html += "			</div>";
		html += "		</div>";
		html += "	</div>";
		html += "</div>";
	}
	
	html += "</div>";
	
	return html;
}

function initOrderData(_mode, _part_order_data) {
	if(_mode == "edit") {
		$('#part_name').val(_part_order_data.part_name);
		$('#part_order_url').val(_part_order_data.part_order_url);
		$('#part_order_quantity').val(_part_order_data.part_order_quantity);
		$('#part_ticket_number').val(_part_order_data.part_ticket_number);
		$('#delivery_option').val(_part_order_data.delivery_option);
		$('#tracking_number').val(_part_order_data.tracking_number);
		$('#tracking_url').val(_part_order_data.tracking_url);
		$('#part_order_staff_name').val(_part_order_data.part_order_staff_name);
		$('#part_order_store_name').val(store_list[_part_order_data.store_id]);
		$('#part_order_price').val(_part_order_data.part_order_price);
		$('#arrival_time').val(_part_order_data.arrival_time);
		$('#total_order_price').val(_part_order_data.total_order_price);
		$('#shipping_cost').val(_part_order_data.shipping_cost);
		$('#transaction_cost').val(_part_order_data.transaction_cost);
		$('#part_order_status').val(_part_order_data.part_order_status);
		$('#duedate').val(_part_order_data.duedate);

		if(_part_order_data.urgent == "Y") {
			$('input:checkbox[id=urgent]').attr("checked", true);
			$(".duedate-wrapper").removeClass("hidden");
		} else {
			$('input:checkbox[id=urgent]').attr("checked", false);
			$(".duedate-wrapper").addClass("hidden");
			$("#duedate").val("");
		}
		
		if(_part_order_data.part_order_status == "Deleted") {
			$('#part_name').attr("readonly", true);
			$('#part_order_url').attr("readonly", true);
			$('#part_order_quantity').attr("readonly", true);
			$('#part_ticket_number').attr("readonly", true);
			$('#delivery_option').attr("readonly", true);
			$('#tracking_number').attr("readonly", true);
			$('#tracking_url').attr("readonly", true);
			$('#transaction_cost').attr("readonly", true);
			$('#part_order_price').attr("readonly", true);
			$('#arrival_time').attr("readonly", true);
			$('#total_order_price').attr("readonly", true);
			$('#shipping_cost').attr("readonly", true);
			$('#part_order_status').attr("disabled", true);
			$('#btnPopupURL').attr("disabled", true);
			$('#btnPopupTrackingURL').attr("disabled", true);
			$('#duedate').attr("readonly", true);
		} else if(_part_order_data.part_order_status == "Processing") {
			$('#part_name').attr("readonly", true);
			$('#part_order_url').attr("readonly", true);
			$('#part_order_quantity').attr("readonly", true);
			$('#part_ticket_number').attr("readonly", true);
			$('#delivery_option').attr("readonly", true);
		} else if(_part_order_data.part_order_status == "Shipping") {
			$('#part_name').attr("readonly", true);
			$('#part_order_url').attr("readonly", true);
			$('#part_order_quantity').attr("readonly", true);
			$('#part_ticket_number').attr("readonly", true);
			$('#part_order_price').attr("readonly", true);
			$('#total_order_price').attr("readonly", true);
			$('#shipping_cost').attr("readonly", true);
			$('#delivery_option').attr("readonly", true);
		} else if(_part_order_data.part_order_status != "New") {
			$('#part_name').attr("readonly", true);
			$('#part_order_url').attr("readonly", true);
			$('#part_order_quantity').attr("readonly", true);
			$('#part_ticket_number').attr("readonly", true);
			$('#part_order_price').attr("readonly", true);
			$('#total_order_price').attr("readonly", true);
			$('#shipping_cost').attr("readonly", true);
			$('#arrival_time').attr("readonly", true);
			$('#delivery_option').attr("readonly", true);
		}
		
		if(staff_info.role == "manager") {
			$('#tracking_number').attr("readonly", true);
			$('#tracking_url').attr("readonly", true);
			$('#transaction_cost').attr("readonly", true);
			$('#part_order_price').attr("readonly", true);
			$('#arrival_time').attr("readonly", true);
			$('#total_order_price').attr("readonly", true);
			$('#shipping_cost').attr("readonly", true);
			
			if(_part_order_data.part_order_status == "Shipping" || _part_order_data.part_order_status == "Received" || _part_order_data.part_order_status == "Draft") {
				$('#part_order_status').attr("disabled", false);
			} else {
				$('#part_order_status').attr("disabled", true);
			}
		} else if(staff_info.role == "technician") {
			$('#tracking_number').attr("readonly", true);
			$('#tracking_url').attr("readonly", true);
			$('#transaction_cost').attr("readonly", true);
			$('#part_order_price').attr("readonly", true);
			$('#arrival_time').attr("readonly", true);
			$('#total_order_price').attr("readonly", true);
			$('#shipping_cost').attr("readonly", true);
			
			if(_part_order_data.part_order_status == "Draft") {
				$('#part_order_status').attr("disabled", false);
			} else {
				$('#part_order_status').attr("disabled", true);
			}
		}
		
		if(_part_order_data.tracking_url.length <= 0) {
			$('#btnPopupTrackingURL').attr("disabled", true);
		}
		
		getStatusMemo(_part_order_data.part_order_pk);
	} else {
		$('#part_order_staff_name').val(staff_info.staff_name);
		$('#part_order_store_name').val(store_list[staff_info.store_id]);
		
		if(staff_info.role == "technician") {
			$('#part_order_status').val("Draft");
		} else {
			$('#part_order_status').val("New");
		}
		
		$('#btnPopupURL').attr("disabled", true);
		$('#btnPopupTrackingURL').attr("disabled", true);
		$('#part_order_status').attr("disabled", true);
	}
}

function getStatusMemo(_part_order_pk) {
	var params = {};
	params.type = "get_status_memo";
	params.part_order_pk = _part_order_pk;
	
	$.post(conf_url_ajax, params, function(_data) {		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			var html = "";
			
			$.each(ret_data.data, function(key, val) {
				html += "<tr>";
				html += "<td>" + val.part_order_status_log_date + "</td>";
				html += "<td>" + val.part_order_status + "</td>";
				html += "<td>" + val.part_order_update_staff_name + "</td>";
				html += "<td class='memo_max_width'>" + val.part_order_memo + "</td>";
				html += "</tr>";
			});
			
			$("#memo_tbody").html(html);
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function generatePartOrderPk() {
	return (new Date().getTime()) + "" + (Math.floor(Math.random() * ((99999 - 10000) + 1) + 10000));
}

function setPartOrderStatus(_part_order_status) {
	var html = "";
	
	$.each(part_order_status, function(key, val) {
		if(_part_order_status) {
			if(_part_order_status == "Shipping") {
				if(val == "Shipping" || val == "Received") {
					html += "<option value='" + val + "'>" + val + "</option>";				
				}
			} else if(_part_order_status == "Received") {
				if(val == "Received" || val == "Defected") {
					html += "<option value='" + val + "'>" + val + "</option>";
				}
			} else if(_part_order_status == "Draft" && staff_info.role == "technician") {
				if(val == "Draft" || val == "Deleted") {
					html += "<option value='" + val + "'>" + val + "</option>";
				}
			} else if(_part_order_status == "Draft" && staff_info.role == "manager") {
				if(val == "Draft" || val == "New" || val == "Deleted") {
					html += "<option value='" + val + "'>" + val + "</option>";
				}
			} else {
				html += "<option value='" + val + "'>" + val + "</option>";
			}
		} else {
			html += "<option value='" + val + "'>" + val + "</option>";
		}
	});
	
	
	$('#part_order_status').html(html);
}

function setPartOrderStatusFilter() {
	var html = "";
	
	html += "<option value='All'>All</option>";
	$.each(part_order_status, function(key, val) {
		html += "<option value='" + val + "'>" + val + "</option>";
	});
	
	$('#part_order_list_status_filter').html(html);
}

function statusFilter(_status) {
	if(_status != "All") {
		list_table.columns(1).search(_status).draw();
	} else {
		list_table.columns(1).search("").draw();
	}
}

function updatePartOrderStatus(_part_order_pk) {
	var params = {};
	params.type = "update_part_order";
	params.part_order_pk = _part_order_pk;
	params.part_name = $.trim($('#part_name').val());
	params.part_order_url = $.trim($('#part_order_url').val());
	params.part_order_quantity = $.trim($('#part_order_quantity').val());
	params.part_ticket_number = $.trim($('#part_ticket_number').val());
	params.delivery_option = $.trim($('#delivery_option').val());
	params.tracking_number = $.trim($('#tracking_number').val());
	params.tracking_url = $.trim($('#tracking_url').val());
	params.transaction_cost = $.trim($('#transaction_cost').val());
	params.part_order_status = $.trim($('#part_order_status').val());
	params.part_order_price = $.trim($('#part_order_price').val());
	params.arrival_time = $.trim($('#arrival_time').val());
	params.total_order_price = $.trim($('#total_order_price').val());
	params.shipping_cost = $.trim($('#shipping_cost').val());
	params.urgent = $('input:checkbox[id=urgent]').is(":checked") ? "Y" : "N";
	params.duedate = $.trim($('#duedate').val());
	params.memo = $.trim($('#memo').val());
	
	if(params.part_order_url.length <= 0) {
		alert("Part Order URL is Required!");
		return;
	}
	
	if(params.part_order_quantity.length <= 0 || params.part_order_quantity <= 0) {
		alert("Part Order Quantity is Required!");
		return;
	}
	
	if(params.part_ticket_number.length <= 0) {
		alert("Ticket Number is Required!");
		return;
	}
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		
		$('#modal_popup').modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			part_order_list[_part_order_pk].part_name = ret_data.data.part_name;
			part_order_list[_part_order_pk].part_order_url = ret_data.data.part_order_url;
			part_order_list[_part_order_pk].part_order_quantity = ret_data.data.part_order_quantity;
			part_order_list[_part_order_pk].part_ticket_number = ret_data.data.part_ticket_number;
			part_order_list[_part_order_pk].delivery_option = ret_data.data.delivery_option;
			part_order_list[_part_order_pk].tracking_number = ret_data.data.tracking_number;
			part_order_list[_part_order_pk].tracking_url = ret_data.data.tracking_url;
			part_order_list[_part_order_pk].part_order_status = ret_data.data.part_order_status;
			part_order_list[_part_order_pk].part_order_price = ret_data.data.part_order_price;
			part_order_list[_part_order_pk].arrival_time = ret_data.data.arrival_time;
			part_order_list[_part_order_pk].total_order_price = ret_data.data.total_order_price;
			part_order_list[_part_order_pk].shipping_cost = ret_data.data.shipping_cost;
			part_order_list[_part_order_pk].transaction_cost = ret_data.data.transaction_cost;
			part_order_list[_part_order_pk].urgent = ret_data.data.urgent;
			part_order_list[_part_order_pk].duedate = ret_data.data.duedate;
			
			updateTableRow(part_order_list[_part_order_pk]);

			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function addNewPartOrder() {
	var params = {};
	params.type = "add_new_part_order";
	params.part_name = $.trim($('#part_name').val());
	params.part_order_url = $.trim($('#part_order_url').val());
	params.part_order_quantity = $.trim($('#part_order_quantity').val());
	params.part_ticket_number = $.trim($('#part_ticket_number').val());
	params.delivery_option = $.trim($('#delivery_option').val());
	params.tracking_number = $.trim($('#tracking_number').val());
	params.tracking_url = $.trim($('#tracking_url').val());
	params.part_order_status = $.trim($('#part_order_status').val());
	params.urgent = $('input:checkbox[id=urgent]').is(":checked") ? "Y" : "N";
	params.memo = $.trim($('#memo').val());
	params.duedate = $.trim($('#duedate').val());
	
	if(params.part_order_url.length <= 0) {
		alert("Part Order URL is Required!");
		return;
	}
	
	if(params.part_order_quantity.length <= 0 || params.part_order_quantity <= 0) {
		alert("Part Order Quantity is Required!");
		return;
	}
	
	if(params.part_ticket_number.length <= 0) {
		alert("Ticket Number is Required!");
		return;
	}
	
	$('.loading').show();
		
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		$('#modal_popup').modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			part_order_list[ret_data.data.part_order_pk] = ret_data.data;
			
			addNewTableRow(ret_data.data);
						
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function deletePartOrder(_part_order_pk) {
	var params = {};
	params.type = "update_part_order_status";
	params.part_order_pk = _part_order_pk;
	params.part_order_status = "Deleted";
	
	$('.loading').show();
	
	$.post(conf_url_ajax, params, function(_data) {
		$('.loading').hide();
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			part_order_list[_part_order_pk].part_order_status = ret_data.data.part_order_status;
			
			updateTableRow(part_order_list[_part_order_pk]);

			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = conf_url_login;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function updateTableRow(_data) {
	var deleteOrderHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteOrder' " + (((staff_info.role == "admin" || _data.part_order_account_pk == staff_info.part_order_account_pk) && _data.part_order_status != "Deleted") ? "" : "disabled") + "><i class='fa fa-times'></i>";
	var urgentHtml = "";

	if(_data.urgent == "Y") {
		urgentHtml = "<span class='urgent'>Urgent</span>";
	}
	
	var update_node = list_table.row($('#' + _data.part_order_pk)).data([
	    _data.part_order_create_date,
	    _data.part_order_status,
	    _data.part_ticket_number,
		_data.part_order_pk,
	    _data.part_name,
	    store_list[_data.store_id],
	    _data.part_order_quantity,
	    _data.delivery_option,
	    urgentHtml,
	    _data.arrival_time,
	    _data.total_order_price,
	    _data.part_order_url,
	    deleteOrderHtml
	]).draw(false).node();
	
	if(_data.urgent == "Y") {
		$(update_node).addClass("urgent-tr");
	} else {
		$(update_node).removeClass("urgent-tr");
	}
}

function addNewTableRow(_data) {
	var deleteOrderHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteOrder' " + (((staff_info.role == "admin" || _data.part_order_account_pk == staff_info.part_order_account_pk) && _data.part_order_status != "Deleted") ? "" : "disabled") + "><i class='fa fa-times'></i>";
	var urgentHtml = "";

	if(_data.urgent == "Y") {
		urgentHtml = "<span class='urgent'>Urgent</span>";
	}
	
	var new_node = list_table.row.add([
	    _data.part_order_create_date,
	    _data.part_order_status,
	    _data.part_ticket_number,
		_data.part_order_pk,
       	_data.part_name,
       	store_list[_data.store_id],
       	_data.part_order_quantity,
       	_data.delivery_option,
       	urgentHtml,
       	_data.arrival_time,
       	_data.total_order_price,
       	_data.part_order_url,
       	deleteOrderHtml
    ]).draw(false).node();

    $(new_node).attr("id", _data.part_order_pk);
    
    if(_data.urgent == "Y") {
		$(new_node).addClass("urgent-tr");
	} else {
		$(new_node).removeClass("urgent-tr");
	}
}
