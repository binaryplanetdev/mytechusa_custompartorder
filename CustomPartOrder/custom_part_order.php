<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CCustomPartOrderManager"));
	
		$session = new CSession();
		$account_info = $session->getLoginData();
			
		if(!isset($account_info)) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		
		$custom_part_order_manager = new CCustomPartOrderManager($mysql_manager->getDb());
		
		if($account_info["role"] == "admin" || $account_info["role"] == "hq_staff") {
			$ret_part_order_list = $custom_part_order_manager->getCustomPartOrderAllList();
		} else {
			$ret_part_order_list = $custom_part_order_manager->getCustomPartOrderList($account_info["store_id"]);
		}
		
		$part_order_list = array();
		if(isset($ret_part_order_list)) {
			foreach ($ret_part_order_list as $row) {
				$part_order_list[$row["part_order_pk"]] = $row;
			}
		}
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/custom_part_order.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var conf_url_login = "<?php echo CONF_URL_LOGIN; ?>";
			var conf_url_ajax = "<?php echo CONF_URL_AJAX; ?>";
			var conf_url_export = "<?php echo CONF_URL_EXPOT; ?>";
			var part_order_list = <?php echo json_encode($part_order_list); ?>;
			var part_order_status = <?php echo json_encode($ORDER_STATUS); ?>;
			var store_list = <?php echo json_encode($STORE_LIST); ?>;
			var staff_info = <?php echo json_encode($account_info); ?>;
			var list_table = null;

			$(function() {
				list_table = $('#part_order_list').DataTable({
					columns: [
						{ className: "minWidth_50" },
						null,
						null,
						null,
						{ className: "concat" },
						null,
						null,
						null,
						null,
						null,
						null,
						{ className: "concat" },
						{ "orderable": false }
					],
					order: [
						[ 0, "desc" ]
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_popup' id='btnAddNewOrder'>Add Part Order</button>";
						html += "</label>";
						
						if(staff_info.role == "admin") {
							html += "<label>";
							html += "<button type='button' class='btn btn-success' id='btnExportExcel' style='margin-left: 10px;'>Export Excel</button>";
							html += "</label>";
						}
						
						$('#part_order_list_length').html(html);
						
						$('#btnAddNewOrder').off("click").on('click', function() {
							showPartOrderPopup();
						});

						$("#btnExportExcel").off("click").on('click', function() {
							window.open(conf_url_export, "_blank");
						});

						$('#part_order_list_filter').prepend("<label style='margin-right: 10px;'>Status Filter : <select id='part_order_list_status_filter' class='form-control'></select></label>");
						
						$('#part_order_list_status_filter').off("change").on('change', function() {
							statusFilter($("#part_order_list_status_filter").val());
						});

						setPartOrderStatusFilter();
					},
					drawCallback: function() {						
						$('#part_order_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#part_order_list > tbody > tr");

							var isOpenPopup = false;
							for(var i = 0; i < 12; i++) {
								var key = '#part_order_list > tbody > tr:eq(' + index + ') > td:eq(' + i + ')';
								if($(event.target).is(key)) {
									isOpenPopup = true;
									break;
								}
							}

							if(isOpenPopup) {
								var part_order_pk = $(this).attr("id");
						        var part_order_data = part_order_list[part_order_pk];
						        
						        $('#modal_popup').modal('show');

						        showPartOrderPopup(part_order_data);
							}
						});

						$(".btnDeleteOrder").off("click").on("click", function() {
							var result = confirm("Are you sure delete selected order?");
							if(result) {
								var part_order_pk = $(this).parent("td").parent("tr").attr("id");
								deletePartOrder(part_order_pk);
							}
						});
					}
				});
				
				$.each(part_order_list, function(key, val) {
					var deleteOrderHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteOrder' " + (((staff_info.role == "admin" || val.part_order_account_pk == staff_info.part_order_account_pk) && val.part_order_status != "Deleted") ? "" : "disabled") + "><i class='fa fa-times'></i></button>";
					var urgentHtml = "";

					if(val.urgent == "Y") {
						urgentHtml = "<span class='urgent'>Urgent</span>";
					}
					
					var new_node = list_table.row.add([
						val.part_order_create_date,
						val.part_order_status,
						val.part_ticket_number,
						val.part_order_pk,
						val.part_name,
						store_list[val.store_id],
						val.part_order_quantity,
						val.delivery_option,
						urgentHtml,
						val.arrival_time,
						val.total_order_price,
						val.part_order_url,
						deleteOrderHtml
					]).node();

					$(new_node).attr("id", val.part_order_pk);

					if(val.urgent == "Y") {
						$(new_node).addClass("urgent-tr");
					} else {
						$(new_node).removeClass("urgent-tr");
					}
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div class="loading">Loading</div>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header"><?php echo $pageTitle; ?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading"><?php echo $pageTitle . " List"; ?></div>
							<div class="panel-body">
								<div class="dataTable_wrapper table-responsive">
									<table class="table table-striped table-bordered table-hover" id="part_order_list">
										<thead>
											<tr>
												<th>Date</th>
												<th>Status</th>
												<th>Ticket Number</th>
												<th>Order ID</th>
												<th>Name</th>
												<th>Store Name</th>
												<th>Quantity</th>
												<th>Delivery Option</th>
												<th>Urgent</th>
												<th>Estimate Arrival Time</th>
												<th>Cost</th>
												<th>URL</th>
												<th>Deletion</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>