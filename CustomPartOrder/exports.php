<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "php-export-data.class", "CMysqlManager", "CCustomPartOrderManager"));
		
		$session = new CSession();
		$account_info = $session->getLoginData();
		
		if(!isset($account_info)) {
			echo bc_return("NOT_LOGIN", "Not Login");
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		
		$custom_part_order_manager = new CCustomPartOrderManager($mysql_manager->getDb());
		
		$exporter = new ExportDataExcel("browser", "custom_part_order_" . time() . ".xls");
		$exporter->initialize(); // starts streaming data to web browser
		
		$ret_part_order_list = $custom_part_order_manager->getCustomPartOrderAllList();
		
		$exporter->addRow(
			array(
				"Date", 
				"Status", 
				"Ticket Number", 
				"Part Order ID", 
				"Part Name", 
				"Store Name", 
				"Quantity", 
				"Delivery Option", 
				"Estimate Arrival Time", 
				"Part Order URL",
				"Unit Price",
				"Total Cost"
			)
		);
		
		foreach ($ret_part_order_list as $row) {
			$exporter->addRow(
				array(
					$row["part_order_create_date"],
					$row["part_order_status"],
					$row["part_ticket_number"],
					$row["part_order_pk"],
					$row["part_name"],
					$STORE_LIST[$row["store_id"]],
					$row["part_order_quantity"],
					$row["delivery_option"],
					$row["arrival_time"],
					$row["part_order_url"],
					$row["part_order_price"],
					$row["total_order_price"]
				)
			);
		}
				
		$exporter->finalize(); // writes the footer, flushes remaining data to browser.
	} catch (Exception $e) {
		
	}
	
	exit;
?>