<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CAccountManager"));
	
		$session = new CSession();
		$account_info = $session->getLoginData();
			
		if(!isset($account_info)) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		if($account_info["role"] != "admin") {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
				
		$mysql_manager = new CMysqlManager();
		
		$account_manager = new CAccountManager($mysql_manager->getDb());
		
		$ret_account_list = $account_manager->getAccountList();
		$account_list = array();
		if(isset($ret_account_list)) {
			foreach ($ret_account_list as $row) {
				$account_list[$row["part_order_account_pk"]] = $row;
			}
		}
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/account.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var conf_url_login = "<?php echo CONF_URL_LOGIN; ?>";
			var conf_url_ajax = "<?php echo CONF_URL_AJAX; ?>";
			var account_list = <?php echo json_encode($account_list); ?>;
			var account_role = <?php echo json_encode($ACCOUNT_ROLE); ?>;
			var store_list = <?php echo json_encode($STORE_LIST); ?>;
			var account_status_list = <?php echo json_encode($ACCOUNT_STATUS); ?>;
			var list_table = null;

			$(function() {
				list_table = $('#account_list').DataTable({
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_popup' id='btnAddAccount'>Add Account</button>";
						html += "</label>";
						$('#account_list_length').html(html);
						
						$('#btnAddAccount').off("click").on('click', function() {
							showAccountPopup();
						});
					},
					drawCallback: function() {						
						$('#account_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#account_list > tbody > tr");

							var isOpenPopup = false;
							for(var i = 0; i < 6; i++) {
								var key = '#account_list > tbody > tr:eq(' + index + ') > td:eq(' + i + ')';
								if($(event.target).is(key)) {
									isOpenPopup = true;
									break;
								}
							}

							if(isOpenPopup) {
								var part_order_account_pk = $(this).attr("id");
						        var account_data = account_list[part_order_account_pk];
						        
						        $('#modal_popup').modal('show');

						        showAccountPopup(account_data);
							}
						});

						$(".btnDeleteAccount").off("click").on("click", function() {
							var result = confirm("Are you sure delete selected account?");
							if(result) {
								var part_order_account_pk = $(this).parent("td").parent("tr").attr("id");
								deleteAccount(part_order_account_pk);
							}
						});
					}
				});
				
				$.each(account_list, function(key, val) {
					var deleteAccountHtml = "<button type='button' class='btn btn-warning btn-circle btnDeleteAccount' " + (val.account_status == "Deleted" ? "disabled" : "") + "><i class='fa fa-times'></i></button>";
					
					var new_node = list_table.row.add([
						val.account_id,
						val.staff_name,
						store_list[val.store_id],
						val.role,
						val.account_status,
						val.create_date,
						deleteAccountHtml
					]).node();

					$(new_node).attr("id", val.part_order_account_pk);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div class="loading">Loading</div>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Account Manage</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Account List</div>
							<div class="panel-body">
								<div class="dataTable_wrapper table-responsive">
									<table class="table table-striped table-bordered table-hover" id="account_list">
										<thead>
											<tr>
												<th>ID</th>
												<th>Staff Name</th>
												<th>Store Name</th>
												<th>Role</th>
												<th>Status</th>
												<th>Create Date</th>
												<th>Deletion</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>