<?php 
	$pageTitle = "Purchase Order";
	if($account_info["role"] == "manager") {
		$pageTitle = "Custom Part Order";
	}
?>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li style="padding: 3px 20px;"><?php echo $account_info["staff_name"] . " [ " . $account_info["role"] . " ]"; ?></li>
				<li class="divider"></li>
				<li><a href="<?php echo CONF_URL_LOGOUT; ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
		</li>
	</ul>
	<div class="navbar-header"><a class="navbar-brand" href="<?php echo CONF_URL_INDEX; ?>">MyTechUSA Custom Part Order</a></div>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li>
					<a href="<?php echo CONF_URL_INDEX; ?>"><i class="fa fa-dashboard fa-fw"></i>Home</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_PART_ORDER_PAGE; ?>"><?php echo $pageTitle; ?></a>
				</li>
<?php
	if($account_info["role"] != "technician") {
?>				
				<li>
					<a href="<?php echo CONF_URL_CASH_DEPOSIT_PAGE; ?>">Cash Deposit</a>
				</li>
<?php
	}
?>
				<li>
					<a href="<?php echo CONF_URL_SKU_PAGE; ?>">SKU</a>
				</li>
<?php
	if($account_info["role"] == "admin") {
?>
				
				<li>
					<a href="<?php echo CONF_URL_ACCOUNT; ?>">Account Manage</a>
				</li>
<?php
	}
?>
			</ul>
		</div>
	</div>
</nav>