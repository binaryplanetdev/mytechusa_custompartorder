<?php
	header("Content-Type:text/html; charset=utf-8");
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession"));
	
		$session = new CSession();	
		$session->logout();
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > Login"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time(); ?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Please Sign In</h3></div>
						<div class="panel-body">
							<form id="login_form" method="post" action="<?php echo CONF_URL_LOGIN_ACTION; ?>" role="form">
								<fieldset>
									<div class="form-group">
										<select class="form-control" name="store" id="store" autofocus></select>
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="ID" name="id" id="id" type="text"/>
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="Password" name="password" id="password" type="password"/>
									</div>
									<!-- Change this to a button or input when using this as a form -->
									<button type="submit" id="login_submit" class="btn btn-lg btn-success btn-block">Sign In</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			var store_list = <?php echo json_encode($STORE_LIST); ?>;
			
			$(document).ready(function() {
				setStoreList();

				$("#login_submit").click(function(event) {
					event.preventDefault();

					var store_id = $.trim($("#store").val());
					var id = $.trim($("#id").val());
					var pw = $.trim($("#password").val());

					if(!store_id || store_id == -1) {
						alert("Select Store");
						$("#store").focus();
						return;
					}

					if(id.length <= 0) {
						alert("ID is Required!");
						$("#id").focus();
						return;
					}

					if(pw.length <= 0) {
						alert("Password is Required!");
						$("#password").focus();
						return;
					}

					$("#login_form").submit();
				});
			});

			function setStoreList() {
				var html = "<option value='-1'>Select Store...</option>";
				$.each(store_list, function(key, val) {
					html += "<option value='" + key + "'>" + val + "</option>";
				});
				
				$('#store').html(html);
			}
		</script>
	</body>
</html>
