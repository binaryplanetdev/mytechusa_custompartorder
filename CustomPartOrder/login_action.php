<?php
	header("Content-Type: text/html; charset=UTF-8");
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CAccountManager"));
	
		$session = new CSession();
		$mysql_manager = new CMysqlManager();
		$accountManager = new CAccountManager($mysql_manager->getDb());
		
		$id = isset($_POST["id"]) ? $_POST["id"] : "";
		$pw = isset($_POST["password"]) ? $_POST["password"] : "";
		$store_id = isset($_POST["store"]) ? $_POST["store"] : -1;
		
		if(!empty($id) && !empty($pw)) {
			$pw = md5($pw);
			
			$account_info = $accountManager->login($id, $pw, $store_id);
			if(!$account_info) {
				echo "<script>";
				echo "alert('Fail to Login.');";
				echo "location.href = '" . CONF_URL_ROOT . "';";
				echo "</script>";
				exit;
			}
			
			$session->login($account_info);
		}

		moveToSpecificPage(CONF_URL_ROOT);
		exit;
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>
