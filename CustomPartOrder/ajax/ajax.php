<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CCustomPartOrderManager", "CAccountManager", "CCustomPartOrderLogManager", "CCashDepositManager","CCashDepositLogManager", 
									"CCashDepositAttachmentManager", "CCashDepositItemsManager"));
	
		$session = new CSession();
		$account_info = $session->getLoginData();
		
		if(!isset($account_info)) {
			echo bc_return("NOT_LOGIN", "Not Login");
			exit;
		}
	
		$mysql_manager = new CMysqlManager();
		
		$custom_part_order_manager = new CCustomPartOrderManager($mysql_manager->getDb());
		$custom_part_order_log_manager = new CCustomPartOrderLogManager($mysql_manager->getDb());
		$cash_deposit_manager = new CCashDepositManager($mysql_manager->getDb());
		$cash_deposit_attachments_manager = new CCashDepositAttachmentManager($mysql_manager->getDb());
		$cash_deposit_log_manager = new CCashDepositLogManager($mysql_manager->getDb());
		$cash_deposit_items_manager = new CCashDepositItemsManager($mysql_manager->getDb());
		
		$account_manager = new CAccountManager($mysql_manager->getDb());
		
		$type = $_POST['type'];
		$ret = array("result" => "OK", "message" => "SUCCESS", "data" => null);
		
		if($type == "add_new_part_order") {
			$part_name = $_POST["part_name"];
			$part_order_url = $_POST["part_order_url"];
			$part_order_quantity = $_POST["part_order_quantity"];
			$part_ticket_number = $_POST["part_ticket_number"];
			$delivery_option = $_POST["delivery_option"];
			$tracking_number = $_POST["tracking_number"];
			$tracking_url = $_POST["tracking_url"];
			$part_order_status = $_POST["part_order_status"];
			$part_order_price = isset($_POST["part_order_price"]) ? strval($_POST["part_order_price"]) : "";
			$arrival_time = isset($_POST["arrival_time"]) ? strval($_POST["arrival_time"]) : "";
			$total_order_price = isset($_POST["total_order_price"]) ? strval($_POST["total_order_price"]) : ""; 
			$shipping_cost = isset($_POST["shipping_cost"]) ? strval($_POST["shipping_cost"]) : "";
			$urgent = isset($_POST["urgent"]) ? $_POST["urgent"] : "N";
			$duedate = isset($_POST["duedate"]) ? $_POST["duedate"] : "";
			$memo = isset($_POST["memo"]) ? $_POST["memo"] : "";
			
			$ret["data"] = $custom_part_order_manager->insertNewPartOrder($account_info["part_order_account_pk"], $account_info["store_id"], $part_name, $part_order_url, $part_order_quantity, $part_ticket_number, 
																			$delivery_option, $tracking_number, $tracking_url, $account_info["staff_name"], $part_order_status, $part_order_price, $arrival_time, 
																			$total_order_price, $shipping_cost, $memo, $urgent, $duedate);
		} else if($type == "update_part_order") {
			$part_order_pk = $_POST["part_order_pk"];
			$part_name = $_POST["part_name"];
			$part_order_url = $_POST["part_order_url"];
			$part_order_quantity = $_POST["part_order_quantity"];
			$part_ticket_number = $_POST["part_ticket_number"];
			$delivery_option = $_POST["delivery_option"];
			$tracking_number = $_POST["tracking_number"];
			$tracking_url = $_POST["tracking_url"];
			$transaction_cost = isset($_POST["transaction_cost"]) ? strval($_POST["transaction_cost"]) : "";
			$part_order_status = $_POST["part_order_status"];
			$part_order_price = isset($_POST["part_order_price"]) ? strval($_POST["part_order_price"]) : "";
			$arrival_time = isset($_POST["arrival_time"]) ? strval($_POST["arrival_time"]) : "";
			$total_order_price = isset($_POST["total_order_price"]) ? strval($_POST["total_order_price"]) : "";
			$shipping_cost = isset($_POST["shipping_cost"]) ? strval($_POST["shipping_cost"]) : "";
			$urgent = isset($_POST["urgent"]) ? $_POST["urgent"] : "N";
			$duedate = isset($_POST["duedate"]) ? $_POST["duedate"] : "";
			$memo = isset($_POST["memo"]) ? $_POST["memo"] : "";
			
			$ret["data"] = $custom_part_order_manager->updatePartOrder($part_order_pk, $part_name, $part_order_url, $part_order_quantity, $part_ticket_number, $delivery_option, $tracking_number, $tracking_url, 
																		$part_order_status, $account_info["part_order_account_pk"], $account_info["staff_name"], $memo, $part_order_price, $arrival_time, 
																		$total_order_price, $shipping_cost, $transaction_cost, $urgent, $duedate);
		} else if($type == "update_part_order_status") {
			$part_order_pk = $_POST["part_order_pk"];
			$part_order_status = $_POST["part_order_status"];
			
			$ret["data"] = $custom_part_order_manager->updatePartOrderStatus($part_order_pk, $part_order_status, $account_info["part_order_account_pk"], $account_info["staff_name"], "");
		} else if($type == "get_status_memo") {
			$part_order_pk = $_POST["part_order_pk"];
			
			$ret["data"] = $custom_part_order_log_manager->getCustomPartOrderLogList($part_order_pk);
		} else if($type == "add_account") {
			$account_id = $_POST["account_id"];
			$account_pw = $_POST["account_pw"];
			$staff_name = $_POST["staff_name"];
			$store_id = $_POST["store_id"];
			$role = $_POST["role"];
			$account_status = $_POST["account_status"];
			
			$ret["data"] = $account_manager->insertAccount($account_id, $account_pw, $staff_name, $store_id, $role, $account_status);
		} else if($type == "update_account") {
			$part_order_account_pk = $_POST["part_order_account_pk"];
			$account_id = $_POST["account_id"];
			$account_pw = $_POST["account_pw"];
			$staff_name = $_POST["staff_name"];
			$store_id = $_POST["store_id"];
			$role = $_POST["role"];
			$account_status = $_POST["account_status"];
			
			$ret["data"] = $account_manager->updateAccount($part_order_account_pk, $account_id, $account_pw, $staff_name, $store_id, $role, $account_status);
		} else if($type == "update_account_status"){
			$part_order_account_pk = $_POST["part_order_account_pk"];
			$account_status = $_POST["account_status"];
				
			$account_manager->updateAccountStatus($part_order_account_pk, $account_status);
		} else if($type == "add_cash_deposit") {
			$cash_deposit_date = $_POST["cash_deposit_date"];
			$deposit_cash = $_POST["deposit_cash"];
			$status = $_POST["status"];
			$memo = isset($_POST["memo"]) ? $_POST["memo"] : "";
			$deposit_list = isset($_POST["deposit_list"]) ? $_POST["deposit_list"] : null;
			
			if($deposit_list != null) {
				$deposit_list = json_decode($deposit_list, true);
			}
			
			$uploaded_paths = array();
			if(isset($_FILES['attachment']) && !empty($_FILES['attachment'])) {
				$attachments_files = $_FILES['attachment'];
				$filenames = $attachments_files['name'];
				
				$attachment_file_path = getAttachmentPath($cash_deposit_date, $account_info["store_id"]);
				$uploaded_paths = moveAttachmentFiles($attachments_files, $filenames, $attachment_file_path);
			}
			
			$ret["data"] = $cash_deposit_manager->insertNewCashDeposit($deposit_cash, $cash_deposit_date, $status, $account_info["part_order_account_pk"], $account_info["staff_name"], $account_info["store_id"], 
																		$memo, $uploaded_paths, $deposit_list);
		} else if($type == "update_cash_deposit") {
			$cash_deposit_pk = $_POST["cash_deposit_pk"];
			$cash_deposit_date = $_POST["cash_deposit_date"];
			$deposit_cash = $_POST["deposit_cash"];
			$status = $_POST["status"];
			$memo = isset($_POST["memo"]) ? $_POST["memo"] : "";
			$deleted_attachment_pks = isset($_POST["deleted_attachment_pks"]) && !empty($_POST["deleted_attachment_pks"]) ? explode(",", $_POST["deleted_attachment_pks"]) : null;
			$deposit_list = isset($_POST["deposit_list"]) ? $_POST["deposit_list"] : null;
			if($deposit_list != null) {
				$deposit_list = json_decode($deposit_list, true);
			}
			
			$uploaded_paths = array();
			if(isset($_FILES['attachment']) && !empty($_FILES['attachment'])) {
				$attachments_files = $_FILES['attachment'];
				$filenames = $attachments_files['name'];
			
				$attachment_file_path = getAttachmentPath($cash_deposit_date, $account_info["store_id"]);
				$uploaded_paths = moveAttachmentFiles($attachments_files, $filenames, $attachment_file_path);
			}
			
			$ret["data"] = $cash_deposit_manager->updateCashDeposit($cash_deposit_pk, $deposit_cash, $cash_deposit_date, $status, $account_info["part_order_account_pk"], $account_info["staff_name"], 
																	$account_info["store_id"], $memo, $deleted_attachment_pks, $uploaded_paths, $deposit_list);
		} else if($type == "update_cash_deposit_status") {
			$cash_deposit_pk = $_POST["cash_deposit_pk"];
			$status = $_POST["status"];
			
			$ret["data"] = $cash_deposit_manager->updateCashDepositStatus($cash_deposit_pk, $status, $account_info["part_order_account_pk"], $account_info["staff_name"], $account_info["store_id"], "");
		} else if($type == "get_cash_deposit_log") {
			$cash_deposit_pk = $_POST["cash_deposit_pk"];
			
			$ret["data"] = $cash_deposit_log_manager->getCashDepositLogList($cash_deposit_pk);
		} else if($type == "get_cash_deposit_items") {
			$cash_deposit_pk = $_POST["cash_deposit_pk"];
			
			$ret["data"] = $cash_deposit_items_manager->getCashDepositItemsList($cash_deposit_pk);
		}
	} catch (Exception $e) {
		echo bc_return("ERROR", $e->getMessage());
		exit;
	}
	
	echo bc_return($ret["result"], $ret["message"], $ret["data"]);
	exit;
	
	function getAttachmentPath($_cash_deposit_date, $_store_id) {
		$convert_date = explode("/", $_cash_deposit_date);
		$year = $convert_date[2];
		$month = $convert_date[0];
		$day = $convert_date[1];
			
		$attachment_file_path = "";
		
		$attachment_file_path .= $year;
		if(!file_exists(CONF_PATH_ATTACHMENTS . $attachment_file_path)) {
			mkdir(CONF_PATH_ATTACHMENTS . $attachment_file_path, 0755);
		}
			
		$attachment_file_path .= "/" . $month;
		if(!file_exists(CONF_PATH_ATTACHMENTS . $attachment_file_path)) {
			mkdir(CONF_PATH_ATTACHMENTS . $attachment_file_path, 0755);
		}
		
		$attachment_file_path .= "/" . $day;
		if(!file_exists(CONF_PATH_ATTACHMENTS . $attachment_file_path)) {
			mkdir(CONF_PATH_ATTACHMENTS . $attachment_file_path, 0755);
		}
		
		$attachment_file_path .= "/" . "store_" . $_store_id;
		if(!file_exists(CONF_PATH_ATTACHMENTS . $attachment_file_path)) {
			mkdir(CONF_PATH_ATTACHMENTS . $attachment_file_path, 0755);
		}
		
		return $attachment_file_path;
	}
	
	function moveAttachmentFiles($_attachments_files, $_filenames, $_attachment_file_path) {
		$uploaded_paths = array();
		for($i = 0; $i < count($_filenames); $i++) {
			$upload_file_path = $_attachment_file_path . "/" . time() . "_" . basename($_attachments_files['name'][$i]);
				
			if (move_uploaded_file($_attachments_files['tmp_name'][$i], CONF_PATH_ATTACHMENTS . $upload_file_path)) {
				$uploaded_paths[] = $upload_file_path;
			} else {
				$uploaded_paths = array();
				echo bc_return("ERROR", "Error while uploading images.");
				exit;
			}
		}
			
		if(empty($uploaded_paths)) {
			echo bc_return("ERROR", "Error while uploading images.");
			exit;
		}
		
		return $uploaded_paths;
	}
?>