<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession"));
		
		$session = new CSession();
		$account_info = $session->getLoginData();
			
		if(isset($account_info)) {
			moveToSpecificPage(CONF_URL_PART_ORDER_PAGE);
		} else {
			moveToSpecificPage(CONF_URL_LOGIN);
		}
		
		exit;
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>
